import React, { Component } from 'react';
import withAuthorization from './withAuthorization';
import {Table, Button, Row, Col, ProgressBar, Modal} from 'react-bootstrap';
import 'brace/mode/java';
import 'brace/theme/github';
import 'brace/theme/monokai';
import AceEditor from 'react-ace';

const textStyle = {
    color:"red",
    fontWeight: "bold"
  }
  
  const textStyleAdded = {
    color:"green",
    fontWeight: "bold"
  }
class CreateActivity extends Component {
    constructor(props) {
          super(props);
          this.state = {activityId: '',
                        numHints: 0,
                        code: '',
                        score:0,
                        show:false,
                        error: null,
                        message: null,
                   userActivity: null};
        this.handleOnClick = this.handleOnClick.bind(this);
        this.handleOnClickAnalyze = this.handleOnClickAnalyze.bind(this);
        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.handleOnClickRestart = this.handleOnClickRestart.bind(this);
        this.handleOnClickSave = this.handleOnClickSave.bind(this);
    }
    
    handleOnClickRestart() {
        this.setState({code:this.state.userActivity.code});
    }

    handleOnClickSave() {
        var url = 'http://localhost:8080/users/'+this.props.location.state.userId+'/updateActivity/'+this.props.match.params.activityId
        console.log(url);
        fetch(url, {
            method: 'PUT',
            body: JSON.stringify({code:this.state.code, fileName:this.state.userActivity.fileName}),
            headers: {
        'content-type': 'application/json'
         },  
        }).then(response => { return response.json();
        });//second fetch 
    }
    handleCodeChange(newValue) {
        this.setState({
          code: newValue,
        });
      }
      componentDidMount() {
          fetch('http://localhost:8080/users/'+this.props.location.state.userId+'/getUserActivity/'+this.props.match.params.activityId, {
            method: 'GET',
            headers: {
        'content-type': 'application/json'
         },  
        }).then(response => { return response.json();
        }).then(data => {
        console.log(data)
        this.setState({code:data.code,numHints:data.lastHint,score:data.score,userActivity:data,activityId:data.activityId});
        });//second fetch 
     }

     handleOnClickAnalyze(event) {
        event.preventDefault();
        this.setState({show:true,error:null,message:null});
        var userId = this.props.location.state.userId;
        var activityId = this.state.activityId;
        console.log()
        fetch('http://localhost:8080/users/'+userId+'/analyze/'+activityId, {
            method: 'PUT',
            body: JSON.stringify({ruleId:this.props.match.params.ruleId, code:this.state.code, fileName:this.state.userActivity.fileName}),
            headers: {
        'content-type': 'application/json'
         },  
        }).then(response => { return response.json();
        }).then(data => {
            if(data.msg) {
                this.setState({error:data, show:false});
            }
            else{
        this.setState({score:data.score, show:false, message:"Analysis finished. Your score is "+data.score});
            }
        }); 
     }
     handleOnClick(event) {
         event.preventDefault();
         var num = this.state.numHints+1;
         this.setState({numHints:num});
         fetch('http://localhost:8080/users/'+this.props.location.state.userId+'/newHint/'+this.state.activityId, {
            method: 'PUT',
            headers: {
        'content-type': 'application/json'
         },  
        }).then(response => { return response.json();
        }).then(data => {
        });
     }
     render() {
         console.log(this.props);
         if(this.state.userActivity!==null) { 
            var hintSize = this.state.userActivity.hints.length;
            const isInvalid =
               this.state.numHints == hintSize ||
               this.state.score == 10
            
            const isInvalid2 =
               this.state.score == 10;
         if(this.state.numHints == 0) {
             var hints = []
         }
         else {
            console.log(this.state.userActivity.hints);
            console.log(this.state.numHints);
                      console.log(this.state.show);

            var allHints = this.state.userActivity.hints;
            console.log(allHints);
            var arrayHints = allHints.slice(0,this.state.numHints);
          console.log(arrayHints);
          var hints = arrayHints.map((hint, index) =>
        <tr><td>{hint.hint}</td> </tr> );
         }

         return( <div>
                  <p> Fixed percentage </p>
              <ProgressBar now={this.state.score*10} label={`${this.state.score*10}%`} />

           <Row>
              <Col md={12}>
              <AceEditor
                    ref={instance => { this.aceEditor = instance; }}
                    mode="java"
                    theme="monokai"
                    name="blah2"
                    onChange={this.handleCodeChange}
                    fontSize={14}
                    width='1500px'
                    showPrintMargin={false}
                    showGutter={true}
                    highlightActiveLine={true}
                    value={this.state.code}
                    setOptions={{
                        enableBasicAutocompletion: false,
                        enableLiveAutocompletion: false,
                        enableSnippets: false,
                        showLineNumbers: true,
                        tabSize: 2,
                }}/>
              </Col>
            </Row>
            { this.state.error && <p style={textStyle}>{this.state.error.msg}</p> }
            { this.state.message && <p style={textStyleAdded}>{this.state.message}</p> }

            <Row>

            <Col smOffset={2} mdOffset={1} md={1} sm={5}>   
            <Button disabled={isInvalid2} onClick = {this.handleOnClickAnalyze}>Analyze and save</Button> 
            </Col>  
            <Col smOffset={2} mdOffset={1} md={1} sm={5}>   
           <Button disabled={isInvalid}  onClick = {this.handleOnClick}>New hint</Button>
           </Col>
           <Col smOffset={2} mdOffset={1} md={1} sm={5}>   
            <Button disabled={isInvalid2} onClick = {this.handleOnClickRestart}> Restart without saving</Button>
            </Col>
           </Row>
          <Table responsive>
            <tbody>
            <tr>
                <th>Hints  </th>
            </tr>
            {hints}
        </tbody>
        </Table>

               <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Analysing activity</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Please wait, the activity is being analyzed
          </Modal.Body>
          </Modal>
         </div>); 
         }
         else {
             return <div></div>;
         }
     }
    }

  const authCondition = (authUser) => !!authUser;
  export default withAuthorization(authCondition)(CreateActivity);