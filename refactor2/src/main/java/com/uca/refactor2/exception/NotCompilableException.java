package com.uca.refactor2.exception;

public class NotCompilableException extends Exception{
	/**
	 * 
	 */
	
	//TODO     compile is not the problem, ask teacher
	private static final long serialVersionUID = 1554804024017327280L;
	private static final String MSG = "The activity does not compile, please fix it";

	@Override
	public String getMessage() {
		return MSG;
}
}
