import React, {Component} from 'react';
import {Form, FormGroup,FormControl,Col,Button, ControlLabel,PageHeader} from 'react-bootstrap';
import { auth } from '../firebase';
import firebase from 'firebase/app';
import ReactDOM from 'react-dom';
const INITIAL_STATE = {
    passwordOne: '',
    passwordTwo: '',
    currentPassword: '',
    message: '',
    error: null
  };

  const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
  

class PasswordChange extends Component {
    constructor(props, context) {
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { ...INITIAL_STATE };
        this.setNewState = this.setNewState.bind(this);
    }

    setNewState() {
        this.setState(() => ({ ...INITIAL_STATE }));
    }
    handleSubmit(event) {
        const {
            passwordOne,
            currentPassword
          } = this.state;
          
          var user = firebase.auth().currentUser;
          const credential = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
          user.reauthenticateWithCredential(credential).then(() => { 
            auth.doPasswordUpdate(passwordOne)
            .then(() => {
                ReactDOM.findDOMNode(this.messageForm).reset();
                this.setState(() => ({ ...INITIAL_STATE }));
                this.setState(byPropKey('message', "Contraseña actualizada"));
             })
            .catch(error => {
                this.setState(byPropKey('error', error));
           });
        }).catch(error => {
            this.setState(byPropKey('error', error));
        });      
        event.preventDefault();
    }

    
    render()  {
        const {
            passwordOne,
            passwordTwo,
            error,
            message
          } = this.state;
      
          const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '';
      
        return(         
            <div>
            
            <Col smOffset={2} mdOffset={1}>    
            <PageHeader>
            <small> Change Password</small>
            </PageHeader>
            </Col>
             <Form horizontal onSubmit = {this.handleSubmit}      ref={ form => this.messageForm = form }>
             <FormGroup onChange = {event => this.setState(byPropKey('currentPassword', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Current Password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Password" />
                    </Col>
                </FormGroup>
                <FormGroup onChange = {event => this.setState(byPropKey('passwordOne', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Password" />
                    </Col>
                </FormGroup>
                <FormGroup onChange = {event => this.setState(byPropKey('passwordTwo', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Confirm password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Confirm password" />
                    </Col>
                </FormGroup>
                
                <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Change my password</Button>
                    </Col>
                </FormGroup>
            </Form>
            { error && <p>{error.message}</p> }
            { message!=='' && <p>{message}</p> }
            </div>
        );
    }
    
}

export default PasswordChange;