package com.uca.refactor2.model.sonar;

public class Period {

    public Integer index;

    public String value;

    @Override
    public String toString() {
	return "Period [index=" + index + ", value=" + value + "]";
    }

}
