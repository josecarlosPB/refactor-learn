package com.uca.refactor2.model.sonar;

public class Periods {

    public Integer index;

    public String mode;

    public String date;

    public String parameter;

    @Override
    public String toString() {
	return "Periods [index=" + index + ", mode=" + mode + ", date=" + date + ", parameter=" + parameter + "]";
    }

}
