import React, {Component} from 'react';
import {PageHeader,Col, Button} from 'react-bootstrap';
import {Editor, EditorState, ContentState,convertToRaw} from 'draft-js';
import DraftPasteProcessor from '../../node_modules/draft-js/lib/DraftPasteProcessor';
class RuleDescriptionViewAdmin extends Component {
    constructor(props,context){
        super(props,context);
        this.handleOnClick = this.handleOnClick.bind(this);

        const processedHTML = DraftPasteProcessor.processHTML(this.props.rule.description.replace(/\n/g, "<br />"));
        const contentState = ContentState.createFromBlockArray(processedHTML); 
        var editorState = EditorState.createWithContent(contentState);
        var editorState = EditorState.moveFocusToEnd(editorState);
        this.state = {editorState: editorState};
        this.onChange = (editorState) => this.setState({editorState});
    }


  
handleOnClick(event) {
    var url = 'http://localhost:8080/rules/' + this.props.rule.id;
    const blocks = convertToRaw(this.state.editorState.getCurrentContent()).blocks;
    const value = blocks.map(block => (!block.text.trim() && '\n') || block.text).join('\n');
    console.log(value)
    console.log(this.props.rule);
    console.log(this.props.rule.activities);

    fetch(url, {
        method: 'PUT',
        body: JSON.stringify({"description":value, "title": this.props.rule.title, "searchName": this.props.rule.searchName, "activities": this.props.rule.activities, "id": this.props.rule.id, "category": this.props.rule.category}),
      headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      console.log(data);
      //TODO, maybe redirect to edi rules again?
    });
    

} 
/*
{rule.split('\n').map((item, key) => {
    return <span key={key}><Col smOffset={2} mdOffset={1} sm={6}>{item}</Col><br/></span>
        })}*/
    render(){

        return(
        <div>

        <Col smOffset={2} mdOffset={1}>
        <PageHeader>
            {this.props.rule.title}
        </PageHeader>
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}
        />
        </Col>
    
         <Col smOffset={2} mdOffset={1}>
        <Button onClick = {this.handleOnClick}>Update rule</Button>
        </Col>
        </div>
        );

    }
}

export default RuleDescriptionViewAdmin;