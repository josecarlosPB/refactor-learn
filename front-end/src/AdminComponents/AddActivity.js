import React, {Component} from 'react';
import fetch from 'isomorphic-fetch';
import AceEditor from 'react-ace';
import {Row, Col, Button, Form, FormGroup, ControlLabel, FormControl, Modal} from 'react-bootstrap';
import 'brace/mode/java';
import 'brace/theme/github';
import 'brace/theme/monokai';

const textStyle = {
  color:"red",
  fontWeight: "bold"
}

const textStyleAdded = {
  color:"green",
  fontWeight: "bold"
}
export default class AddActivity extends Component {

 
  // Add context
  constructor(props, context) {
    super(props, context);
    this.state = {
      code: "Enter the code here",
      activityId: null,
      name: "",
      error: null,
      show: false,
      message: null,
      difficulty: 0.5
    };
    this.handleCodeChange = this.handleCodeChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleOnChangeName = this.handleOnChangeName.bind(this);
    this.handleOnChangeDiff = this.handleOnChangeDiff.bind(this);
  }

      handleOnChangeName(event) {
        this.setState({name: event.target.value});
      }

      
      handleOnChangeDiff(event) {
        this.setState({difficulty: event.target.value});
      }



  render() {

    var difficulty = parseFloat(this.state.difficulty);
    console.log(difficulty);
    const isInvalid =
    this.state.name === '' ||
    difficulty===null ||
    difficulty<1 ||
    difficulty>10||
    isNaN(difficulty) ;


    var error = this.state.error;
    return (
      <div>
        <Row>
          <Col md={12}>
          <AceEditor
                ref={instance => { this.aceEditor = instance; }}
                mode="java"
                theme="monokai"
                name="blah2"
                onChange={this.handleCodeChange}
                fontSize={14}
                width='1500px'
                showPrintMargin={false}
                showGutter={true}
                highlightActiveLine={true}
                value={this.state.code}
                setOptions={{
                    enableBasicAutocompletion: false,
                    enableLiveAutocompletion: false,
                    enableSnippets: false,
                    showLineNumbers: true,
                    tabSize: 2,
            }}/>
          </Col>
        </Row>
        <br></br>
        { error && <p style={textStyle}>{error.msg}</p> }
        { this.state.message && <p style={textStyleAdded}>{this.state.message}</p> }
        <Form horizontal onSubmit = {this.handleCreate}>
                <FormGroup onChange = {this.handleOnChangeName} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Activity Name
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="Activity name" />
                    </Col>
                </FormGroup>
                <FormGroup onChange = {this.handleOnChangeDiff} controlId="formHorizontalDifficulty">
                    <Col componentClass={ControlLabel} sm={2}>
                    Difficulty (1-10)
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="Write the difficulty here, it can be decimal" />
                    </Col>
                </FormGroup>
                 <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Create activity</Button>
                    </Col>
                </FormGroup>
        </Form>


            <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add activity</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Please wait, the activity and hints are being created 
          </Modal.Body>
          </Modal>
      </div>
    );
  }

  handleCodeChange(newValue) {
    this.setState({
      code: newValue,
    });
  }

  
  
  // we need to attach the activity to the rule, so we need to make a DTO and pass rule id as attribute 
  //this create activity will be different from the create activity of users, because this should generate hints, so we need 
  // initial info of sonarqube, we can return the new created Activity because it'll have code+hints, thats all we need. 
  // with id we should be sure that after we get the issues, we should UPDATE that new activity and not create another one.

  //TODO    catch to add error message*


  handleCreate(event) {
    this.setState({show: true, message:null,error:null});
    var url = 'http://localhost:8080/activity/'+ this.props.match.params.id + '/addActivity';
        fetch(url, {
      method: 'POST',
      body: JSON.stringify({ "code": this.state.code, "name": this.state.name, "difficulty":parseFloat(this.state.difficulty) }),
      headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      if(data.msg) {
        this.setState({error:data, show:false});
        console.log(this.state.error);
      }
      else {
      this.setState({
        activityId: data.id,
        show: false,
        message: "The activity has been added!" 
     });  
      }
    });
  
    event.preventDefault();
  }

}
