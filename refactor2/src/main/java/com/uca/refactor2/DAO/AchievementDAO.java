package com.uca.refactor2.DAO;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.Achievement;
import com.uca.refactor2.model.User;

@Repository
public interface AchievementDAO extends CrudRepository<Achievement, Integer>  {
	
	Achievement findOne(Integer id);
	
	List<Achievement> findAll();
	
	Achievement save (Achievement achievement);
	
	void delete(Achievement achivement);
	
	Achievement findAchievementByName(String name);
	
	void deleteAllInBatch();
	
	
}
