package com.uca.refactor2.service;

import java.util.List;

import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.JoinedRuleActivity;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.RuleDTO;


public interface RuleService{

Rule findOne(Integer id);
	
	List<Rule> findAll();
	
	List<Activity>  findActivities(Integer ruleId);
	
	Rule save (Rule Rule);
	
	void delete(Rule achivement);
	Rule update(RuleDTO rule);
	Rule findRuleByTitle(String title);
	
}
