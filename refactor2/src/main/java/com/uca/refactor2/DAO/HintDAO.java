package com.uca.refactor2.DAO;



import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.Hint;
import com.uca.refactor2.model.Rule;
	@Repository
	public interface HintDAO extends CrudRepository<Hint, Integer>  {
		
		Hint findOne(Integer id);
			
		Hint save (Hint Hint);
		List<Hint> findAll();		
	}

