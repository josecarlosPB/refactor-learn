/* NavBar will change depending the user type (teacher, student) so it needs to be dynamic navbar or 2 different components*/

import React, {Component} from 'react';
import {Navbar, NavItem, Nav} from 'react-bootstrap'
import AuthUserContext from './AuthUserContext';
import SignOutButton from './SignOut';
import * as routes from '../constants/routes';
import { withRouter } from 'react-router-dom';
const Navigation = ({history}) =>

/*
  <AuthUserContext.Consumer>
       { authUser => 
       authUser
      ? <NavigationAuth />
      : <NavigationNonAuth />
    }
  </AuthUserContext.Consumer> */

/*
    decideRender = (authUser) =>
    authUser.getIdTokenResult()
    .then((idTokenResult) => {
       if (!!idTokenResult.claims.student) {
         console.log("user")
       } else {
         console.log("not user");
       }
    })
    .catch((error) => {
      console.log(error);
    });
  */
  
  <AuthUserContext.Consumer>
  { authUser => 
  authUser
 ? <NavigationAuth authUser = {authUser} history={history}/>
 : <NavigationNonAuth history = {history}/>
}
</AuthUserContext.Consumer>


class NavigationAuth extends Component {
constructor(props,context) {
    super(props,context);
    this.state = ({value:0,gotUserRole:false});
}


   render() {
       console.log(this.state.gotUserRole);
       console.log(this.state.value);
     if(this.state.gotUserRole === false) {
        this.props.authUser.getIdTokenResult(true) //this is a PROMISE item
        .then((idTokenResult) => {
            console.log(idTokenResult.claims.student);
            if (!!idTokenResult.claims.student) {
                // eslint-disable-next-line
                console.log("I'm a student")
                this.setState({value:1,gotUserRole:true})
                return <div></div>
     
            } else if(!!idTokenResult.claims.admin){
                // eslint-disable-next-line
                console.log("I'm not a student")
                this.setState({value:2,gotUserRole:true})
                return <div></div>
              }
              else if(!!idTokenResult.claims.teacher){
                // eslint-disable-next-line
                console.log("I'm a teacher")
                this.setState({value:3,gotUserRole:true})
                return <div></div>
              }
         })
         .catch((error) => {
           return <NavigationNonAuth history = {this.props.history}/>
         });
         return <NavigationNonAuth history = {this.props.history}/>
     }
    else if(this.state.value===1) {
        console.log("Now on student value");
        return <NavigationStudent history = {this.props.history}/>;
    }
    else if(this.state.value===2) {
        console.log("Now on admin value");
        return <NavigationAdmin history = {this.props.history}/>;
    }
    else if(this.state.value===3) {
        console.log("Now on teacher value");
        return <NavigationAdmin history = {this.props.history}/>;
    }
    else {
        console.log("not known role");
        return <NavigationNonAuth history = {this.props.history}/>
    }
   }

    
}


class NavigationAdmin extends React.Component {
    render() {
    return(
    <Navbar inverse collapseOnSelect>
    <Navbar.Header>
        <Navbar.Brand>
            <a onClick={() => this.props.history.push(routes.HOME)}>Refactoring</a>
        </Navbar.Brand>
        <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
    <Nav >
        <NavItem eventKey={1} onClick={() => this.props.history.push(routes.ALL_USERS)}>
            List Users
        </NavItem>
        <NavItem eventKey={2} onClick={() => this.props.history.push(routes.ADMIN_RULES)}>
            List Rules
        </NavItem>
        <NavItem eventKey={3} onClick={() => this.props.history.push(routes.RANKING)}>
            Ranking
        </NavItem>

    </Nav>
    <Nav pullRight>
        <NavItem eventKey={1} onClick={() => this.props.history.push(routes.NEW_TEACHER)}>
            Create teacher
        </NavItem>
        <NavItem eventKey={2} onClick={() => this.props.history.push(routes.ACCOUNT)}>
            Account
        </NavItem>
        <NavItem eventKey={3} >
            <SignOutButton/>
        </NavItem>
 
    </Nav>
    </Navbar.Collapse>
    </Navbar>);
    }
}
    

class NavigationStudent extends React.Component {


render() {
return(
<Navbar inverse collapseOnSelect>
<Navbar.Header>
    <Navbar.Brand>
        <a onClick={() => this.props.history.push(routes.LANDING)}>Refactoring</a>
    </Navbar.Brand>
    <Navbar.Toggle />
</Navbar.Header>
<Navbar.Collapse>
<Nav>
<NavItem eventKey={1} onClick={() => this.props.history.push(routes.ALL_RULES)}>
        Rules
    </NavItem>
    <NavItem eventKey={2} onClick={() => this.props.history.push(routes.RANKING)}>
            Ranking
        </NavItem>
</Nav>
<Nav pullRight>
    <NavItem eventKey={1} onClick={() => this.props.history.push(routes.ACCOUNT)}>
        Account
    </NavItem>
    <NavItem eventKey={2} >
            <SignOutButton/>
    </NavItem>
 

</Nav>
</Navbar.Collapse>
</Navbar>);
}
}

    
class NavigationNonAuth extends React.Component {

render() {
return (
<Navbar inverse collapseOnSelect>
<Navbar.Header>
    <Navbar.Brand>
        <a onClick={() => this.props.history.push(routes.LANDING)}>Refactoring</a>
    </Navbar.Brand>
    <Navbar.Toggle />
</Navbar.Header>
<Navbar.Collapse>
<Nav pullRight>

    <NavItem eventKey={1} onClick={() => this.props.history.push(routes.SIGN_IN)}>
        Sign in
    </NavItem>
    <NavItem eventKey={2} onClick={() => this.props.history.push(routes.SIGN_UP)}>
        Sign up
    </NavItem>
</Nav>
</Navbar.Collapse>
</Navbar>);
} }
export default withRouter(Navigation);