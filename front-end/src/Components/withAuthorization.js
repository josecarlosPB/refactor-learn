import React from 'react';
import { withRouter } from 'react-router-dom';

import AuthUserContext from './AuthUserContext';
import { firebase } from '../firebase';
import * as routes from '../constants/routes';

//we have to decide if this run slow, or there is a small bug
const withAuthorization = (authCondition, neededRole) => (Component) => {
  class WithAuthorization extends React.Component {
    constructor(props) {
      super(props);
      this.state = {authenticated: false};
    }
    componentDidMount() {
      firebase.auth.onAuthStateChanged(authUser => {
        if (!authCondition(authUser)) {
          this.props.history.push(routes.SIGN_IN);
        }
        else if(neededRole=="ADMIN") {
          authUser.getIdTokenResult(true) 
          .then((idTokenResult) => {
              if(!idTokenResult.claims.admin){
                 this.props.history.push(routes.SIGN_IN);
                }
              else {
                this.setState({authenticated:true});
              }
              })
            }
          else if(neededRole=="TEACHER") {
            authUser.getIdTokenResult(true) 
            .then((idTokenResult) => {
                if(!idTokenResult.claims.teacher && !idTokenResult.claims.admin){ //No teacher or admin
                   this.props.history.push(routes.SIGN_IN);
                  }
                  else {
                    this.setState({authenticated:true});
                  }
                })
              }
           else if(neededRole=="NONE") {
             this.setState({authenticated:true})                          
           }

      });
    }

    render() {

      return (
        <AuthUserContext.Consumer>
          {authUser => authUser ? <Component {  ...this.props} /> : null}
        </AuthUserContext.Consumer>
      );
    
    }
  }

  return withRouter(WithAuthorization);
}

export default withAuthorization;