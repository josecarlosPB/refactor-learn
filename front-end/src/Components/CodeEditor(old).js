import React, {Component} from 'react';

import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/java';
import 'brace/theme/github';
import 'brace/theme/monokai';

export default class CodeEditor extends Component {

    render() {
        let onChange = this.props.onChange;
        return (
            <AceEditor
                mode="java"
                theme="monokai"
                name="blah2"
                onLoad={this.onLoad}
                onChange={onChange}
                fontSize={14}
                showPrintMargin={true}
                showGutter={true}
                highlightActiveLine={true}
                value={`Enter your code here...`}
                setOptions={{
                    enableBasicAutocompletion: false,
                    enableLiveAutocompletion: false,
                    enableSnippets: false,
                    showLineNumbers: true,
                    tabSize: 2,
            }}/>
        );
    }

    onChange() {
        console.log('I changed');
    }
}