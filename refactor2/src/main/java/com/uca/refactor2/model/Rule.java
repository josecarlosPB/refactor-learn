package com.uca.refactor2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="RULE")
public class Rule implements Serializable {

	private static final long serialVersionUID = -1328491895725244351L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	private String title;
	private String category;
	@Column(name = "DESCRIPTION", length = 65535, columnDefinition="Text" )
	private String description;
	private String searchName;

	public Rule() {}
	public Rule(Integer id) {
		this.id = id;
	}
	public Rule(String title, String category, String description, String searchName) {
		this.title  = title;
		this.category = category;
		this.description = description;
		this.setSearchName(searchName);
	}

	 @OneToMany(cascade=CascadeType.ALL, mappedBy="rule",  fetch=FetchType.EAGER)
	 private List<Activity> ListActivities = new ArrayList<Activity>();
	 
	 
	 
	  
	public List<Activity> getActivities() {
	    		return ListActivities;
	    }
	 
	 public void addActivity(Activity activity) {
	        // Notice a JoinedUserAchievement object
	        ListActivities.add(activity);
	    }
	 
	 public void setActivities(List<Activity> activities) {
		 ListActivities  = activities;
	 }
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSearchName() {
		return searchName;
	}
	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	

}
