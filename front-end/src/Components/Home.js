import React from 'react'
import { Carousel } from "react-bootstrap";
/*
  <Carousel>
  <Carousel.Item>
    <img width={900} height={500} alt="900x500" src="/foto.jpg" />
    <Carousel.Caption>
      <h3>Learn about refactoring java rules</h3>
      <p>You can choose which rule you want to learn about from our rule list.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img width={900} height={500} alt="900x500" src="../images/carousel.png" />
    <Carousel.Caption>
      <h3>Resolve the exercises on our integrated code editor</h3>
      <p>Simple but effective code editor to solve our exercises.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img width={900} height={500} alt="900x500" src="../images/carousel.png" />
    <Carousel.Caption>
      <h3>Get achievements</h3>
      <p>You can get achievements solving exercises and rank up on our ranking!.</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
)

*/
var imgStyle = {
    width: "100%",
    height: "100%"
}
var textStyle = {
  color:"black",
  fontWeight: "bold"
}
const Home = () => (

  <Carousel>
  <Carousel.Item>
    <img style={imgStyle} src="https://i.imgur.com/63hB0mo.jpg" />
    <Carousel.Caption>
      <h3 style = {textStyle}>Learn about refactoring java rules</h3>
      <p style = {textStyle}>You can choose which rule you want to learn about from our rule list.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img  style={imgStyle}  src="https://i.imgur.com/GV6c6Tc.png" />
    <Carousel.Caption>
      <h3 style = {textStyle}>Resolve the exercises on our integrated code editor</h3>
      <p style = {textStyle}>Simple but effective code editor to solve our exercises.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img  style={imgStyle} src="../images/carousel.png" />
    <Carousel.Caption>
      <h3 style = {textStyle}>Get achievements</h3>
      <p style = {textStyle}>You can get achievements solving exercises and rank up on our ranking!.</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>


)




export default Home
