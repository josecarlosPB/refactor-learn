import React, {Component} from 'react';
import {Link,   withRouter} from 'react-router-dom';
class ActivityAdmin extends Component{

	render() {	
		return (
                <tr>
                    <td><Link to={this.props.location.pathname + '/edit/' + this.props.activity.id}>{this.props.activity.name}</Link></td>      
                </tr>
		);
	}
}
 
export default withRouter(ActivityAdmin)