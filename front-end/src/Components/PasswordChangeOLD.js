import React, {Component} from 'react';
import {Form, FormGroup,FormControl,Col,Button, ControlLabel,PageHeader} from 'react-bootstrap';
import { auth } from '../firebase';

const INITIAL_STATE = {
    passwordOne: '',
    passwordTwo: '',
    error: null
  };

  const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
  

class PasswordChange extends Component {
    constructor(props, context) {
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { ...INITIAL_STATE };
    }
    handleSubmit(event) {
        const {
            passwordOne,
          } = this.state;
          
          auth.doPasswordUpdate(passwordOne)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
          })
          .catch(error => {
            this.setState(byPropKey('error', error));
          });
    
        event.preventDefault();
        
    }

    
    render()  {
        const {
            passwordOne,
            passwordTwo,
            error,
          } = this.state;
      
          const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '';
      
        return(         
            <div>
            
            <Col smOffset={2} mdOffset={1}>    
            <PageHeader>
             Change Password
            </PageHeader>
            </Col>
             <Form horizontal onSubmit = {this.handleSubmit}>
                <FormGroup onChange = {event => this.setState(byPropKey('passwordOne', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Password" />
                    </Col>
                </FormGroup>
                <FormGroup onChange = {event => this.setState(byPropKey('passwordTwo', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Confirm password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Confirm password" />
                    </Col>
                </FormGroup>
                
                <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Change my password</Button>
                    </Col>
                </FormGroup>
            </Form>
            { error && <p>{error.message}</p> }
            </div>
        );
    }
    
}

//export default PasswordChange;