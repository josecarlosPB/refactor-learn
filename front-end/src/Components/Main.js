import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

import Home from './Home';
import SignUp from './SignUp';
import SignIn from './SignIn';
import ListAllUsers from './ListAllUsers';
import ListAchievements from './ListAchievements';
import ListRules from './ListRules';
import RuleDescription from './RuleDescription';
import * as routes from '../constants/routes';
import withAuthentication from './withAuthentication';
import Account from './Account';
import PasswordForget from './PasswordForget';
class Main extends Component {

    render() {
    return(
        <main>
            <Switch>
            <Route exact path={routes.LANDING} component={Home}/>
            <Route exact path={routes.SIGN_UP} component={SignUp}/>
            <Route exact path={routes.SIGN_IN} component={SignIn}/>
            <Route exact path={routes.ACCOUNT} component={Account}/>
            <Route exact path={routes.PASSWORD_FORGET} component={PasswordForget}/>
            <Route exact path={routes.ALL_RULES} component={ListRules}/>
            <Route path={routes.SPECIFIC_RULE} render={props => <RuleDescription {... props} />} />
            <Route exact path={routes.ALL_USERS} component = {ListAllUsers}/>
            <Route path={routes.USER_ACHIEVEMENTS} render={props => <ListAchievements {...props} /> } />
                
            </Switch>
        </main>);
    }
}
export default withAuthentication(Main);