package com.uca.refactor2.service;

import java.util.List;

import com.uca.refactor2.model.Achievement;

public interface AchievementService {

	Achievement findOne(Integer id);

    List<Achievement> findAll();

    Achievement save(Achievement achievement);

    void delete(Achievement achievement);

    Achievement findAchievementByName(String name);
}
