package com.uca.refactor2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.JoinedUserActivityDAO;
import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.JoinedUserActivity.JoinedUserActivityId;


@Service
public class JoinedUserActivityServiceImpl implements JoinedUserActivityService {
	@Autowired
	private JoinedUserActivityDAO joinedUserActivityDao;
	
	@Override
	public JoinedUserActivity findOne(JoinedUserActivityId id) {
		return joinedUserActivityDao.findOne(id);
	}
	@Override
    public JoinedUserActivity save(JoinedUserActivity joinedUserActivity) {
		return joinedUserActivityDao.save(joinedUserActivity);
	}
	@Override
	public List<JoinedUserActivity> getActivitiesByActivity(Integer userId, Integer ruleId) {
		return joinedUserActivityDao.findActivitiesOfRule(userId, ruleId);
	}
	
	@Override
	public List<JoinedUserActivity> countActivities(Integer userId)  {
		return joinedUserActivityDao.countActivities(userId);
	}
	@Override
	public List<JoinedUserActivity> countActivitiesCompleted(Integer userId)  {
		return joinedUserActivityDao.countActivitiesFinished(userId);
	}
}
