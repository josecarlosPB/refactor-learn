import React,  {Component} from 'react';
import * as routes from '../constants/routes';
import {  withRouter} from 'react-router-dom';
class DeleteUserAdmin extends Component {

	componentDidMount()  {
        var route = 'http://localhost:8080/users/'+this.props.match.params.id;
        fetch(route, {
        method: 'DELETE',
        headers: {
        'content-type': 'application/json'
      },
    }).then() 
    {this.props.history.push(routes.LANDING);
    }
    }

    render() {
        return <div></div>
    }
    }

export default withRouter(DeleteUserAdmin)