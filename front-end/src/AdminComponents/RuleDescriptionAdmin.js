import React, {Component} from 'react';
import RuleDescriptionViewAdmin from './RuleDescriptionViewAdmin';
class RuleDescriptionAdmin extends Component {
    constructor(props) {
		super(props);
		this.state = {rule: []};
	}

	componentDidMount()  {
        var route = 'http://localhost:8080/rules/'+this.props.match.params.id;
        fetch(route, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({rule:data});
      console.log(data);
      console.log(data.activities)
    });
	}


    render() {
        // eslint-disable-next-line
        if(this.state.rule == ''){
            return <div></div>
        }
        return(
        <RuleDescriptionViewAdmin rule = {this.state.rule}/>);
        }
    }

export default RuleDescriptionAdmin;