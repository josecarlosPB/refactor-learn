package com.uca.refactor2.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.JoinedUserAchievementDAO;
import com.uca.refactor2.model.JoinedUserAchievement;
import com.uca.refactor2.model.JoinedUserAchievement.JoinedUserAchievementId;


@Service
public class JoinedUserAchievementServiceImpl implements JoinedUserAchievementService {
	@Autowired
	private JoinedUserAchievementDAO joinedUserAchievementDao;
	
	@Override
	public JoinedUserAchievement findOne(JoinedUserAchievementId id) {
		return joinedUserAchievementDao.findOne(id);
	}

}
