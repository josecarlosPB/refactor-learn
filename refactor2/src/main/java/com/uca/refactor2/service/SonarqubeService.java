package com.uca.refactor2.service;

import org.apache.maven.shared.invoker.MavenInvocationException;

import com.uca.refactor2.model.sonar.MeasureOld;
import com.uca.refactor2.model.sonar.ProjectSearchResponse;
import com.uca.refactor2.model.sonar.issue.IssuesResponse;

public interface SonarqubeService {

	void runAnalysis() throws MavenInvocationException;

	IssuesResponse getIssuesFromFileName(String fileName, String ruleSearchName);

	void setTags(String url);

	ProjectSearchResponse getProjectSearch();

	String getNumberOfCodeLine(String fileName);
}
