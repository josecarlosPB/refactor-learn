import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import Rule from './Rule';
class RulesList extends Component{
	render() {
        console.log(this.props.rules);
		var rules = this.props.rules.map((rule, index) =>
			<Rule key ={index}  rule={rule}/>
    );
		return (
			<Table responsive>
                <tbody>
					<tr>
						<th>Rule name  </th>
						<th>Rule category</th>
					</tr>
					{rules}
				</tbody>
			</Table>
		);
	}
}
export default RulesList;