import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import CountItem from './CountItem';
class Count extends Component{
	render() {
        console.log(this.props.count);
		var count = this.props.count.map((count, index) =>
			<CountItem key ={index}  count={count}/>
    );
		return (
			<Table responsive>
                <tbody>
					<tr>
						<th>Rule title</th>
						<th>Activities</th>
					</tr>
					{count}
				</tbody>
			</Table>
		);
	}
}
export default Count;