package com.uca.refactor2.model.sonar.issue;

import java.util.Collection;

public class Issue {

	public String key;

	public String component;

	public String project;

	public String rule;

	public String status;

	public String resolution;

	public String severity; // !

	public String message; // !

	public Integer line; // !

	public String hash;

	public String author;

	public String effort; // !

	public String creationDate;

	public String updateDate;

	public Collection<String> tags; // !

	public String type;

	public Collection<Comment> comments;

	// Skipping some params

	public TextRange textRange;

	public Collection<Flow> flows;

	// Skipping some params

}
