package com.uca.refactor2.service;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.RuleDAO;
import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.JoinedRuleActivity;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.RuleDTO;

@Service
public class RuleServiceImpl implements RuleService{

	@Autowired
	private RuleDAO ruleDao;

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public Rule findOne(Integer id) {
		log.info("Find Rule with id = " + id);
		return ruleDao.findOne(id);
	}
	
	@Override
	public List<Activity> findActivities(Integer ruleId){
		Rule r = ruleDao.findOne(ruleId);
		return r.getActivities();
	}
	
	@Override
	public List<Rule> findAll() {
		log.info("Find all Rules");
		return ruleDao.findAll();
	}

	@Override
	public Rule save(Rule rule) {
		if (rule.getId() == null) {
			// Not persisted yet
			log.info("Create Rule with name = " + rule.getTitle());
		} else {
			log.info("Update Rule with id = " + rule.getId());
		}
		return ruleDao.save(rule);
	}
	
	@Override
	public Rule update(RuleDTO ruleDTO) {
		Rule rule = ruleDao.findOne(ruleDTO.getId());
		rule.setDescription(ruleDTO.getDescription());
		return ruleDao.save(rule);
	}

	@Override
	public void delete(Rule rule) {
		log.info("Delete Rule with id = " + rule.getId());
		ruleDao.delete(rule);
	}

	@Override
	public Rule findRuleByTitle(String title) {
		log.info("Find Rule with title = " + title);
		return ruleDao.findRuleByTitle(title);
	}

}