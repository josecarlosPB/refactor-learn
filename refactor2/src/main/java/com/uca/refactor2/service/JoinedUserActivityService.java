package com.uca.refactor2.service;

import java.util.List;

import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.JoinedUserActivity.JoinedUserActivityId;

public interface JoinedUserActivityService {

    public JoinedUserActivity findOne(JoinedUserActivityId id);
	

    public JoinedUserActivity  save(JoinedUserActivity joinedUserActivity);


	List<JoinedUserActivity> getActivitiesByActivity(Integer userId, Integer ruleId);


	List<JoinedUserActivity> countActivities(Integer userId);
	List<JoinedUserActivity> countActivitiesCompleted(Integer userId);
}
