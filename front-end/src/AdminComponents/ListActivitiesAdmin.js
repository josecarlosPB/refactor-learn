import React, {Component} from 'react';
import ActivitiesListAdmin from './ActivitiesListAdmin';
class ListActivitiesAdmin extends Component {
    constructor(props) {
		super(props);
		this.state = {activities: []};
	}

	componentDidMount()  {
        var route = 'http://localhost:8080/rules/' + this.props.match.params.id + '/activities';
        console.log(route);
        fetch(route, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({activities:data});
      console.log(data);
    });
	}


    render() {
           // eslint-disable-next-line
           if(this.state.activities === []){
            return <div></div>
        }
        return(
        <ActivitiesListAdmin activities = {this.state.activities}/>);
        }
    }

export default ListActivitiesAdmin;