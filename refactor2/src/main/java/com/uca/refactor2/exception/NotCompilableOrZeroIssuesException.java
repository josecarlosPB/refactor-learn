package com.uca.refactor2.exception;

public class NotCompilableOrZeroIssuesException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1963332498328569388L;
	private static final String MSG = "The activity does not compile or has zero issues of the rule, please fix it";

	@Override
	public String getMessage() {
		return MSG;
}
}
