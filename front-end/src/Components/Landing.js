import React, {Component} from 'react';
import AuthUserContext from './AuthUserContext';
import withAuthorization from './withAuthorization';
import AchievementList from './AchivementList';
import Count from './Count';
import {Col,Row} from 'react-bootstrap'
class Landing extends Component {
    constructor(props) {
		super(props);
        this.state = {user:[],
            count: [],
        achievements:[]};
	}

	componentDidMount() {
        console.log(this.props.authUser.email);
        
        var url = 'http://localhost:8080/users/search/' + this.props.authUser.email+ '/';
        console.log(url);
        fetch(url, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },  
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({user:data});
      console.log(data);
      var route = 'http://localhost:8080/users/'+this.state.user.id+'/achievements';
      fetch(route, {
      method: 'GET',
      headers: {
      'content-type': 'application/json'
    },
  }).then(response => { return response.json();
  }).then(data => {
    this.setState({achievements:data});
    console.log(data);
  });
      var url = 'http://localhost:8080/activity/count/' + this.state.user.id;
      console.log(url);
      fetch(url, {
      method: 'GET',
      headers: {
      'content-type': 'application/json'
    },  
  }).then(response => { return response.json();
  }).then(data => {
    this.setState({count:data});
    console.log(data); 
        });
    });

    
   } 


    render() {
        return(
            <Row>
            <Col md={6} sm={12}>
            <AchievementList achievements = {this.state.achievements}/>
            </Col>
            <Col md={6} sm={12}>
            <Count count = {this.state.count}/>
            </Col>
            </Row>
            
        );

        }
    }

    const LandingI = () => (
        <AuthUserContext.Consumer>
          {authUser =>
            <Landing authUser={authUser} />
          }
        </AuthUserContext.Consumer>
      )       
            
const authCondition = (authUser) => !!authUser
const roleNeeded = "NONE";

export default withAuthorization(authCondition, roleNeeded)(LandingI);