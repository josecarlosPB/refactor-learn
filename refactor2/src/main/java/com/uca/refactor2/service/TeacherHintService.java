package com.uca.refactor2.service;


import java.util.List;
import com.uca.refactor2.model.TeacherHint;

public interface TeacherHintService {
	TeacherHint findOne(Integer id);
	
	List<TeacherHint> findAll();
	
	TeacherHint save (TeacherHint teacherHint);
	void delete(TeacherHint teacherHint);


	
}

