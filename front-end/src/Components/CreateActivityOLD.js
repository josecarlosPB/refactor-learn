import React, { Component } from 'react';
import AuthUserContext from './AuthUserContext';
import withAuthorization from './withAuthorization';
import {Modal, Table, Button, Row, Col} from 'react-bootstrap';
import 'brace/mode/java';
import 'brace/theme/github';
import 'brace/theme/monokai';
import AceEditor from 'react-ace';

class CreateActivityShow extends Component {
    constructor(props) {
          super(props);
          this.state = {userId: '',
                        activityId: '',
                        show: true,
                        numHints: 0,
                        code: '',
                        score:0,
                   userActivity: null};
        this.handleOnClick = this.handleOnClick.bind(this);
        this.handleCodeChange = this.handleCodeChange.bind(this);
    }
    
    
    handleCodeChange(newValue) {
        this.setState({
          code: newValue,
        });
      }
      componentDidMount() {
    
          var url = 'http://localhost:8080/users/search/' + this.props.email+ '/';
          fetch(url, {
          method: 'GET',
          headers: {
          'content-type': 'application/json'
        },  
      }).then(response => { return response.json();
      }).then(data => {
        this.setState({userId:data.id});
        console.log(data);
        console.log(this.props);
        
        fetch('http://localhost:8080/users/'+this.state.userId+'/addActivity/'+this.props.match.params.activityId, {
            method: 'POST',
            headers: {
            'content-type': 'application/json'
          },  
        }).then(response => { return response.json();
        }).then(data => {
          console.log(this.state.show);
          this.setState({userActivity:data, show:false, code:data.code});
          console.log(data)
        });//second fetch 
      }); //first fetch .then
     }  //DidMont

     handleOnClick(event) {
         event.preventDefault();
         console.log(this.state.numHints);
         var num = this.state.numHints+1;
         this.setState({numHints:num});

     }
     render() {
         if(this.state.userActivity!==null) { 
            var hintSize = this.state.userActivity.hints.length;
            const isInvalid =
               this.state.numHints == hintSize;
         if(this.state.numHints == 0) {
             var hints = []
         }
         else {
            console.log(this.state.userActivity.hints);
            console.log(this.state.numHints);
                      console.log(this.state.show);

            var allHints = this.state.userActivity.hints;
            console.log(allHints);
            var arrayHints = allHints.slice(0,this.state.numHints);
          console.log(arrayHints);
          var hints = arrayHints.map((hint, index) =>
        <tr><td>{hint.hint}</td> </tr> );
         }

         return( <div>
             
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>Add activity</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Please wait, the activity is being created
            </Modal.Body>
          </Modal>


           <Row>
              <Col md={12}>
              <AceEditor
                    ref={instance => { this.aceEditor = instance; }}
                    mode="java"
                    theme="monokai"
                    name="blah2"
                    onChange={this.handleCodeChange}
                    fontSize={14}
                    width='1500px'
                    showPrintMargin={false}
                    showGutter={true}
                    highlightActiveLine={true}
                    value={this.state.code}
                    setOptions={{
                        enableBasicAutocompletion: false,
                        enableLiveAutocompletion: false,
                        enableSnippets: false,
                        showLineNumbers: true,
                        tabSize: 2,
                }}/>
              </Col>
            </Row>

           <Button disabled={isInvalid}  onClick = {this.handleOnClick}>New hint</Button>
          <Table responsive>
            <tbody>
            <tr>
                <th>Hints  </th>
            </tr>
            {hints}
        </tbody>
        </Table>
         </div>); 
         }
         else {
             return <div></div>;
         }
     }
    }
/*
  export default props =>  (
    <AuthUserContext.Consumer>
      {authUser =>
        <CreateActivityShow {...props} email={authUser.email}/>
      }
    </AuthUserContext.Consumer>
  )
*/

   const CreateActivity = (props) => (
    <AuthUserContext.Consumer>
      {authUser =>
        <CreateActivityShow  email={authUser.email} {...props}/>
      }
    </AuthUserContext.Consumer>
  )
  

  //const authCondition = (authUser) => !!authUser;
  //export default withAuthorization(authCondition)(CreateActivity);