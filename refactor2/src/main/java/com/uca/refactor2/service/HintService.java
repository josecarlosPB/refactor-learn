package com.uca.refactor2.service;

import java.util.List;


import com.uca.refactor2.model.Hint;

public interface HintService {
	Hint findOne(Integer id);
	
	List<Hint> findAll();
	
	Hint save (Hint Hint);
	

	
}

