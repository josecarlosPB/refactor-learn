package com.uca.refactor2.model.sonar;

import java.util.Collection;

public class MeasureOld {

    public String metric;

    public Integer value; // Can be null in "new violations"

    public Collection<Period> periods;

    @Override
    public String toString() {
	return "Measure [metric=" + metric + ", value=" + value + ", periods=" + periods + "]";
    }

}
