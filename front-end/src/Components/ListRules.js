import React, {Component} from 'react';
import RulesList from './RulesList';
class ListRules extends Component {
    constructor(props) {
		super(props);
		this.state = {rules: []};
	}

	componentDidMount()  {
        var route = 'http://localhost:8080/rules';
        fetch(route, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
        console.log(data);
        data.sort((a,b) => {
            return a.title > b.title;
        });
      this.setState({rules:data});
      console.log(data);
    });
	}


    render() {
        
        return(
        <RulesList rules = {this.state.rules}/>);
        }
    }

export default ListRules;