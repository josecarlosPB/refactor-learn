package com.uca.refactor2.service;

public interface SecureRandomService {

    String randomAlphanumericalString();

}
