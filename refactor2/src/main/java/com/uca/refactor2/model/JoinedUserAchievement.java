
package com.uca.refactor2.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;




@Entity
@Table(name="USER_ACHIEVEMENT")
public class JoinedUserAchievement {

    public JoinedUserAchievement() {}

    public JoinedUserAchievement(JoinedUserAchievementId joinedUserAchievementId, Date dateOfAcquisition) {
        this.joinedUserAchievementId = joinedUserAchievementId;
        this.dateOfAcquisition = dateOfAcquisition;
    }
    
    @ManyToOne
    @JoinColumn(name="ACHIEVEMENT_ID", insertable=false, updatable=false)
    private Achievement achievement;
    @Column(nullable=false)
    private Date dateOfAcquisition;
    
    public String getDate()  {
    	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    	Date date = dateOfAcquisition;
    	return dateFormat.format(date); 
    }
    //nombre get es lo que devuelve en el json!
    
 
    public Achievement getAchievement() {
    	return achievement;
    }
    
    public void setAchievement(Achievement achievement) {
    	this.achievement = achievement;
    }

    @EmbeddedId
    private JoinedUserAchievementId joinedUserAchievementId;
    
    
    // required because JoinedUserAchievments contains composite id
    @Embeddable
    public static class JoinedUserAchievementId implements Serializable {

        /**
		 * 
		 */
		private static final long serialVersionUID = -9180674903145773104L;
		
		
		@ManyToOne
        @JoinColumn(name="USER_ID")
        private User user;
		
        @ManyToOne
        @JoinColumn(name="ACHIEVEMENT_ID")
        private Achievement achievement;
        
        // required no arg constructor
        public JoinedUserAchievementId() {}

        public JoinedUserAchievementId(User user, Achievement achievement) {
            this.user = user;
            this.achievement = achievement;
        }

        public JoinedUserAchievementId(Integer userId, Integer achievementId) {
            this(new User(userId), new Achievement(achievementId));
        }

        public User getUser() {
        	return user;
        }

        
        public Achievement getAchievement() {
        	return achievement;
        }
        
        public void setUser(User user) {
        	this.user = user;
        }
        
        public void setAchievement(Achievement achievement) {
        	this.achievement = achievement;
        }
        @Override
        
        public boolean equals(Object instance) {
            if (instance == null)
                return false;

            if (!(instance instanceof JoinedUserAchievementId))
                return false;

            final JoinedUserAchievementId other = (JoinedUserAchievementId) instance;
            if (!(user.getId().equals(other.getUser().getId())))
                return false;

            if (!(achievement.getId().equals(other.getAchievement().getId())))
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 47 * hash + (this.user != null ? this.user.hashCode() : 0);
            hash = 47 * hash + (this.achievement != null ? this.achievement.hashCode() : 0);
            return hash;
        }
    }
   }
    