package com.uca.refactor2.service;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.uca.refactor2.DAO.UserDAO;
import com.uca.refactor2.exception.NotCompilableException;
import com.uca.refactor2.model.Achievement;
import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.model.JoinedUserAchievement;
import com.uca.refactor2.model.JoinedUserAchievement.JoinedUserAchievementId;
import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.JoinedUserActivity.JoinedUserActivityId;
import com.uca.refactor2.model.User;
import com.uca.refactor2.model.UserType;
import com.uca.refactor2.model.sonar.Analyse;
import com.uca.refactor2.model.sonar.MeasureOld;
import com.uca.refactor2.model.sonar.issue.Issue;

@Service
public class UserServiceImpl implements UserService {
	private final String ISSUE_FIXED = "CLOSED";
	@Autowired
	private UserDAO userDao;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private RuleService ruleService;
	@Autowired
	private AchievementService achievementService;
	@Autowired
	private FileService fileService;
	@Autowired
	private SonarqubeService sonarqubeService;
	@Autowired
	private JoinedUserAchievementService joinedUserAchievementService;
	@Autowired
	private JoinedUserActivityService joinedUserActivityService;
	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	private final String activitiesPath = "activities/";
	//private final String activitiesPath = "src/main/java/com/uca/refactor2/activities/";
	//private final String activitiesPath = "C:\\Users\\carli\\eclipse-workspace\\refactor2\\src\\main\\java\\com\\uca\\refactor2\\activities/";
	private static final Integer FIRST_ACTIVITY = 1;
	private static final Integer TEN_ACTIVITIES = 2;
	private static final Integer FIFTY_ACTIVITIES = 3;
	private static final Integer FIRST_NOHINTS = 4;
	private static final Integer TEN_NOHINTS = 5;
	private static final Integer FIFTY_NOHINTS = 6;
	private static final Integer THREEROW_NOHINTS = 7;
	private static final Integer THREEROW_FIRSTTRY =8;
	private static final Integer THREEROW_PERFECT = 9;
	@Override
    public ActivityDTO analyzeActivity(Integer userId, Integer activityId, ActivityDTO activityDTO) throws MavenInvocationException, IOException, InterruptedException, NotCompilableException {
		Collection<Analyse> analyses = sonarqubeService.getProjectSearch().analyses;
		Iterator<Analyse> iterator = analyses.iterator();
		String firstKey  = iterator.next().key;
		System.out.println(activityDTO.getFileName());
		String oldCode = fileService.readFile(activitiesPath + activityDTO.getFileName());
		try {
			fileService.createFile(activitiesPath, activityDTO.getFileName(), activityDTO.getCode());
		} catch (IOException e) {
			e.printStackTrace();
		}
		//we don't need here to set code activity to null because our code is on activityDTO
		//we now should get the hints with sonarqube
		sonarqubeService.runAnalysis();
		String newKey;
		int tries = 0;
		boolean notCompile = false;
		do { //now we're being sure that we're retrieving the new analysis and not the old one
			//this is expected to be infinite if we send a non compile code, so we have to do something about this
			Collection<Analyse> analyses2 = sonarqubeService.getProjectSearch().analyses;
			Iterator<Analyse> iterator2 = analyses2.iterator();
			newKey = iterator2.next().key;
			System.out.println(tries);
			 TimeUnit.SECONDS.sleep(6);
			 tries = tries+1;
			 if(tries==10) {
				 notCompile = true;
			 }
		} while(newKey.equals(firstKey) && tries<10);
		if(notCompile) {
			try {
				fileService.createFile(activitiesPath, activityDTO.getFileName(), oldCode);
			} catch (IOException e) {
				e.printStackTrace();
			}
			throw new NotCompilableException();
		}
		//we have to test something before, this is a 'soft error' 
		String ncloc = sonarqubeService.getNumberOfCodeLine(activityDTO.getFileName());
		System.out.println("Number of lines " + ncloc);
		String error = "0";
		if(ncloc.equals(error)) {
			throw new NotCompilableException(); 
		}
		Rule rule = ruleService.findOne(activityDTO.getRuleId());
		Collection<Issue> issues = sonarqubeService.getIssuesFromFileName(activityDTO.getFileName(), rule.getSearchName()).issues ;
		int issuesSize = issues.size();
		Iterator<Issue> iterator2 = issues.iterator();
	    int issuesFixed = 0;
		int issuesChecked =0;
	    int numberOfIssues = activityService.findOneActivity(activityId).getNumberOfIssues();
	    System.out.println("Number of Issues "+ numberOfIssues);
	    System.out.println("Issues size "+ issuesSize);
		int cont = 0;
		while (iterator2.hasNext()) {
			String status = iterator2.next().status;
		    if(cont>=issuesSize-numberOfIssues) {
		    	if(status.equals(ISSUE_FIXED)) {
		    		issuesFixed = issuesFixed+1;
		    	}
		    	issuesChecked  =  issuesChecked+1;
		    }
		    cont=cont+1;
		}
		float newScore = 0;
		System.out.println("IssuesFixed "+ issuesFixed);
		newScore=(float)issuesFixed/numberOfIssues;
		newScore = newScore*10;
		activityDTO.setScore(newScore);
		JoinedUserActivity userActivity = joinedUserActivityService.findOne(new JoinedUserActivityId(userId,activityId));
		userActivity.setScore(newScore);
		userActivity.setNumberOfTries(userActivity.getNumberOfTries()+1);
		User user = userDao.findOne(userId);
		if(newScore==10) {//if we completed the activity we're gonna check if the user unlocked any achievement
			System.out.println("on first if, activity completed");
			user.setActivitiesCompleted(user.getActivitiesCompleted()+1); //he has finished one activity more so we have to update that.
			System.out.println("hints used " + userActivity.getLastHint());
			System.out.println("tries used +" + userActivity.getNumberOfTries());
			if(userActivity.getLastHint().equals(0)) { //if he didnt use any hint
				System.out.println("on second if, 0 hints used");
				user.setActivitiesCompletedWithoutHints(user.getActivitiesCompletedWithoutHints()+1); //we update the number of exercised without hins
				user.setActivitiesWithoutHintsInARow(user.getActivitiesWithoutHintsInARow()+1);
			}
			else { //He didn't finished the activity without hints so he loses the streak
				user.setActivitiesWithoutHintsInARow(0);
			}
			if(userActivity.getNumberOfTries().equals(1)) { //if he finished the activity at first try
				user.setActivitiesFirstTryInARow(user.getActivitiesFirstTryInARow()+1);
			}
			else {//if he didn't he also lose the streak
				user.setActivitiesFirstTryInARow(0);
			}
			if(userActivity.getNumberOfTries().equals(1) && userActivity.getLastHint().equals(0)) { //if he did a perfect activity 1+ to the streak
				user.setActivitiesPerfectInARow(user.getActivitiesPerfectInARow()+1);
			}
			else {//if he didn't he lose the streak
				user.setActivitiesPerfectInARow(0);
			}
			User user2 = userDao.save(user);
			//now we have to check if he deserves any achievement
			System.out.println("Activities completed " + user2.getActivitiesCompleted());
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,FIRST_ACTIVITY))==null && user2.getActivitiesCompleted().equals(1)) {
				Achievement ach1 = achievementService.findOne(FIRST_ACTIVITY);
				user2.addAchievement(ach1);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,TEN_ACTIVITIES))==null && user2.getActivitiesCompleted().equals(10)) {
				Achievement ach2 = achievementService.findOne(TEN_ACTIVITIES);
				user2.addAchievement(ach2);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,FIFTY_ACTIVITIES))==null && user2.getActivitiesCompleted().equals(50)) {
				Achievement ach3 = achievementService.findOne(FIFTY_ACTIVITIES);
				user2.addAchievement(ach3);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,FIRST_NOHINTS))==null && user2.getActivitiesCompletedWithoutHints().equals(1)) {
				Achievement ach4 = achievementService.findOne(FIRST_NOHINTS);
				user2.addAchievement(ach4);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,TEN_NOHINTS))==null && user2.getActivitiesCompletedWithoutHints().equals(10)) {
				Achievement ach5 = achievementService.findOne(TEN_NOHINTS);
				user2.addAchievement(ach5);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,FIFTY_NOHINTS))==null && user2.getActivitiesCompletedWithoutHints().equals(50)) {
				Achievement ach6 = achievementService.findOne(FIFTY_NOHINTS);
				user2.addAchievement(ach6);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,THREEROW_NOHINTS))==null && user2.getActivitiesWithoutHintsInARow().equals(3)) {
				Achievement ach7 = achievementService.findOne(THREEROW_NOHINTS);
				user2.addAchievement(ach7);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,THREEROW_FIRSTTRY))==null && user2.getActivitiesFirstTryInARow().equals(3)) {
				Achievement ach8 = achievementService.findOne(THREEROW_FIRSTTRY);
				user2.addAchievement(ach8);
			}
			if(joinedUserAchievementService.findOne(new JoinedUserAchievementId(userId,THREEROW_PERFECT))==null && user2.getActivitiesPerfectInARow().equals(3)) {
				Achievement ach9 = achievementService.findOne(THREEROW_PERFECT);
				user2.addAchievement(ach9);
			}
			userDao.save(user2);
		}
		joinedUserActivityService.save(userActivity);
		
		return activityDTO;
		
	}
	//this is to create a new user-activity
	@Override
	public ActivityDTO addActivity(Integer userId, Integer activityId) throws IOException, InterruptedException, MavenInvocationException {
		User user = userDao.findOne(userId);
		Activity activity = activityService.findOneActivity(activityId);
		ActivityDTO activityDTO = new ActivityDTO();
		String path = activitiesPath + activity.getFileName(); 
		String code = fileService.readFile(path);
		String filename = "U" + Integer.toString(userId)+ activity.getFileName();
		try {
			fileService.createFile(activitiesPath, filename, code);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(activity.getId());
		
		user.addActivity(activity,filename); //we add here the new created activity
		userDao.save(user);
		activityDTO.setFileName(filename); //now we create the DTO that we're going to send to the User
		activityDTO.setCode(code);
		activityDTO.setActivityId(activityId);
		activityDTO.setHints(activity.getHints());
		//we need previous sonarqube data (so we can know if issues are FIXED, so we need to run an analysis of the new created activity
		sonarqubeService.runAnalysis(); //we analyze the new activity
		/*
		Collection<Issue> issues = sonarqubeService.getIssuesFromFileName(filename, activity.getRule().getSearchName()).issues ;
		int cont = 0;
		while (issues.size() == 0 && cont <=10) {
			 issues = sonarqubeService.getIssuesFromFileName(filename, activity.getRule().getSearchName()).issues ;
			 TimeUnit.SECONDS.sleep(6);
			 cont  = cont+1;
			 System.out.println("we are here on while loop");
		}*/
		return activityDTO;
		
	}
	@Override
	public User findOne(Integer id) {
		log.info("Find user with id = " + id);
		return userDao.findOne(id);
	}

	@Override
	public List<User> findAll() {
		log.info("Find all users");
		return userDao.findAll();
	}
	
	@Override

	public List<JoinedUserAchievement> getAchievements(Integer id) {
		User user = userDao.findOne(id);
		return user.getAllAchievement();
	}
	@Override
	public User save(User user) {
		if (user.getId() == null) {
			// Not persisted yet
		} else {
			log.info("Update user with id = " + user.getId());
		}
		return userDao.save(user);
	}

	@Override
	public void delete(User user) {
		log.info("Delete user with id = " + user.getId());
		userDao.delete(user);
	}

	@Override
	public User findUserByEmail(String email) {
		log.info("Find user with username = " + email);
		User u  =  userDao.findByEmail(email);
		System.out.println(email);
		System.out.println(u.getFirstName());
		return u;
	}

	@Override
	public ActivityDTO getUserActivity(Integer userId, Integer activityId) throws IOException {
		JoinedUserActivity userActivity = joinedUserActivityService.findOne(new JoinedUserActivityId(userId,activityId));
		ActivityDTO activity = new ActivityDTO();
		Activity a = activityService.findOneActivity(activityId);
		activity.setActivityId(activityId);
		activity.setLastHint(userActivity.getLastHint());
		activity.setFileName(userActivity.getFileName());
		activity.setHints(a.getHints());
		activity.setScore(userActivity.getScore());
		String path = activitiesPath + userActivity.getFileName(); 
		String code = fileService.readFile(path);
		activity.setCode(code);
		return activity;
	}

	@Override
	public ActivityDTO updateActivity(Integer userId, Integer activityId, ActivityDTO activityDTO) {
		JoinedUserActivity userActivity = joinedUserActivityService.findOne(new JoinedUserActivityId(userId,activityId));
		try {
			fileService.createFile(activitiesPath, userActivity.getFileName(), activityDTO.getCode());
		} catch (IOException e) {
			e.printStackTrace();
		}
		joinedUserActivityService.save(userActivity);
		return activityDTO;
	}

	@Override
	public List<JoinedUserActivity> getActivitiesOfRule(Integer userId, Integer ruleId) {
		return joinedUserActivityService.getActivitiesByActivity(userId, ruleId);

	}

	@Override
	public void newHint(Integer userId, Integer activityId) {
		JoinedUserActivity userActivity = joinedUserActivityService.findOne(new JoinedUserActivityId(userId,activityId));
		userActivity.setLastHint(userActivity.getLastHint()+1);
		joinedUserActivityService.save(userActivity);	
	}
	
	@Override
	public List<User> getUserByType(UserType userType){
		return userDao.findByUserType(userType);
	}
	@Override
	public void createAdminUser(String uid) throws FirebaseAuthException {
		User user = new User();
		user.setUserType(UserType.ADMIN);
		user.setEmail("refactoringuca@gmail.com");
		user.setFirstName("refactor");
		user.setLastName("admin");
		user.setActivitiesCompleted(0);
		user.setActivitiesCompletedWithoutHints(0);
		user.setActivitiesFirstTryInARow(0);
		user.setActivitiesPerfectInARow(0);
	    Map<String, Object> claims = new HashMap<>();
	    claims.put("admin", true); 
		FirebaseAuth.getInstance().setCustomUserClaims(uid, claims);
		userDao.save(user);
	}
	@Override
	public List<User> getAllStudents() {
		return userDao.findByUserType(UserType.STUDENT);
	}

}