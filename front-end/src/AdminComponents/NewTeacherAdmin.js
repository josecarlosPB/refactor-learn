import React, {Component} from 'react';
import {Form, FormGroup,FormControl,Col,Button, ControlLabel,PageHeader} from 'react-bootstrap';
import fetch from 'isomorphic-fetch';
import {   withRouter} from 'react-router-dom';
import * as routes from '../constants/routes';
import { auth } from '../firebase';

const INITIAL_STATE = {
    email: '',
    passwordOne: '',
    passwordTwo: '',
    firstName: '',
    lastName: '',
    error: null,
  };

  const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
  

class NewTeacherAdmin extends Component {
    constructor(props, context) {
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { ...INITIAL_STATE };
    }
    handleSubmit(event) {
        const {
            email,
            passwordOne,
            firstName,
            lastName
          } = this.state;
          
        console.log(email);
        auth.doCreateUserWithEmailAndPassword(email, passwordOne)
        .then(authUser => {
        this.setState(() => ({ ...INITIAL_STATE }));
        authUser.user.updateProfile({
            displayName: firstName + " "+lastName
          })

          authUser.user.getIdToken(false).then(function(idToken) {

            
            
            fetch('http://localhost:8080/users/teacher', {
                method: 'POST',
                body: JSON.stringify({"email":email, "firstName": firstName, "lastName": lastName,
                "idToken": idToken}),
              headers: {
                'content-type': 'application/json'
              },
            }).then(response => { return response.json();
            }).then(data => {
              console.log(data);
            });


            console.log(idToken);
          }).catch(function(error) {
            this.setState(byPropKey('error',error));
          });
                   


        this.props.history.push(routes.LANDING)
        })
        .catch(error => {
        this.setState(byPropKey('error', error));
        });
        event.preventDefault();
    }

    
    render()  {
        const {
            email,
            passwordOne,
            passwordTwo,
            firstName,
            lastName,
            error,
          } = this.state;
      
          const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            firstName === '' ||
            lastName === '';
        return(         
            <div>
            
                
            <Col smOffset={2} mdOffset={1}>    
            <PageHeader>
             New teacher register form
            </PageHeader>
            </Col>
             <Form horizontal onSubmit = {this.handleSubmit}>
                <FormGroup onChange = {event => this.setState(byPropKey('email', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Email
                    </Col>
                    <Col sm={6}>
                    <FormControl type="email" placeholder="email" />
                    </Col>
                </FormGroup>

                <FormGroup onChange = {event => this.setState(byPropKey('firstName', event.target.value))} controlId="formHorizontalFirstName">
                    <Col componentClass={ControlLabel} sm={2}>
                    First name
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="First name" />
                    </Col>
                </FormGroup>

                <FormGroup onChange = {event => this.setState(byPropKey('lastName', event.target.value))} controlId="formHorizontalLastName">
                    <Col componentClass={ControlLabel} sm={2}>
                    Last name
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="Last name" />
                    </Col>
                </FormGroup>

                <FormGroup onChange = {event => this.setState(byPropKey('passwordOne', event.target.value))} controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                    Password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Password" />
                    </Col>
                </FormGroup>

                <FormGroup onChange = {event => this.setState(byPropKey('passwordTwo', event.target.value))} controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                    Confirm password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Confirm password" />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Create teacher user</Button>
                    </Col>
                </FormGroup>
            </Form>
            { error && <p>{error.message}</p> }
            </div>
        );
    }
    
}

export default withRouter(NewTeacherAdmin);
