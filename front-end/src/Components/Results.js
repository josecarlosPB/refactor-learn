import React from 'react';

const Results = (props) => {
  if (props.issues != null) {
    let issuesList = props.issues.map(issue => <li>{issue.message}</li>);
    return (
      <ul>
        {issuesList}
      </ul>
    );
  } 
}

export default Results;