import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
        apiKey: "INSERT API KEY",
        authDomain: "refactoring-144a9.firebaseapp.com",
        databaseURL: "https://refactoring-144a9.firebaseio.com",
        projectId: "refactoring-144a9",
        storageBucket: "",
        messagingSenderId: "688505324840"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}


const auth = firebase.auth();

export {
  auth,
};
