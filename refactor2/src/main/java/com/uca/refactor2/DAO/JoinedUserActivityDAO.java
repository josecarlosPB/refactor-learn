package com.uca.refactor2.DAO;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.JoinedUserActivity.JoinedUserActivityId;

@Repository
public interface JoinedUserActivityDAO extends CrudRepository<JoinedUserActivity, JoinedUserActivityId> {

	@Override
    JoinedUserActivity findOne(JoinedUserActivityId id);
	
    @Override
    JoinedUserActivity  save(JoinedUserActivity joinedUserActivity);
    

    @Query(value = "select distinct jua from JoinedUserActivity jua where jua.joinedUserActivityId.user.id = :userId and jua.activity.rule.id = :ruleId")
    public List<JoinedUserActivity> findActivitiesOfRule(@Param("userId") Integer userId, @Param("ruleId") Integer ruleId);
    
    @Query(value = "select jua.activity.rule.title, jua.activity.rule.id, COUNT(*) from JoinedUserActivity jua where jua.joinedUserActivityId.user.id = :userId and jua.score=10 GROUP BY jua.activity.rule.id  ")
    public List<JoinedUserActivity> countActivitiesFinished(@Param("userId") Integer userId);
    
    @Query(value = "select jua.activity.rule.title, jua.activity.rule.id, COUNT(*) from JoinedUserActivity jua where jua.joinedUserActivityId.user.id = :userId  GROUP BY jua.activity.rule.id  ")
    public List<JoinedUserActivity> countActivities(@Param("userId") Integer userId);
}
