import React, {Component} from 'react';
import {PageHeader,Col, Button, Form, FormGroup, FormControl,ControlLabel} from 'react-bootstrap';
import {Editor, EditorState, ContentState,convertToRaw} from 'draft-js';
import DraftPasteProcessor from '../../node_modules/draft-js/lib/DraftPasteProcessor';

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
class AddRule extends Component {
    constructor(props,context){
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);

        const processedHTML = DraftPasteProcessor.processHTML("Enter rule description here");
        const contentState = ContentState.createFromBlockArray(processedHTML); 
        var editorState = EditorState.createWithContent(contentState);
        var editorState = EditorState.moveFocusToEnd(editorState);
        this.state = {editorState: editorState,
        title: "",
        searchName: "",
        category: ""};
        this.onChange = (editorState) => this.setState({editorState});

    }


  
handleSubmit(event) {
    console.log("hi");
    var url = 'http://localhost:8080/rules/add';
    const blocks = convertToRaw(this.state.editorState.getCurrentContent()).blocks;
    const value = blocks.map(block => (!block.text.trim() && '\n') || block.text).join('\n');
    console.log(url);

    fetch(url, {
        method: 'POST',
        body: JSON.stringify({"description":value, "title": this.state.title, "searchName": this.state.searchName, "category": this.state.category}),
      headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      console.log(data);
      //TODO, maybe redirect to edi rules again?
    });
    event.preventDefault();

} 
/*
{rule.split('\n').map((item, key) => {
    return <span key={key}><Col smOffset={2} mdOffset={1} sm={6}>{item}</Col><br/></span>
        })}*/
    render(){
        console.log("hi");
        const blocks = convertToRaw(this.state.editorState.getCurrentContent()).blocks;
        const value = blocks.map(block => (!block.text.trim() && '\n') || block.text).join('\n');
    var isInvalid = this.state.searchName == "" || this.state.category == "" || value == "" || this.state.title == "" ;
        return(
        <div>

        <Col smOffset={2} mdOffset={1}>
        <PageHeader>
            Description
        </PageHeader>
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}
        />
        </Col>
        <Form horizontal onSubmit = {this.handleSubmit}>
                <FormGroup onChange = {event => this.setState(byPropKey('title', event.target.value))} controlId="formHorizontaltitle">
                    <Col componentClass={ControlLabel} sm={2}>
                    Title
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="Title" />
                    </Col>
                </FormGroup>

                <FormGroup onChange = {event => this.setState(byPropKey('category', event.target.value))} controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                    Category
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="Category" />
                    </Col>
                </FormGroup>
                <FormGroup onChange = {event => this.setState(byPropKey('searchName', event.target.value))} controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                    Search Name
                    </Col>
                    <Col sm={6}>
                    <FormControl type="text" placeholder="Search Name" />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Add Rule</Button>
                    </Col>
                </FormGroup>
            </Form>    
        </div>
        );

    }
}

export default AddRule;