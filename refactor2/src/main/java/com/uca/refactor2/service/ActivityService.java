package com.uca.refactor2.service;

import java.io.IOException;
import java.util.Collection;

import org.apache.maven.shared.invoker.MavenInvocationException;

import com.uca.refactor2.exception.NotCompilableOrZeroIssuesException;
import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.model.sonar.issue.Issue;

public interface ActivityService {

	Activity addActivity(ActivityDTO activityDTO, Integer ruleId) throws MavenInvocationException, InterruptedException, NotCompilableOrZeroIssuesException;
	//Activity create(Activity activity);

	//Collection<Issue> analyze(Integer id);

	//Collection<Issue> analyze(Activity activity);
	Activity findOneActivity(Integer id);
	Collection<Activity> findAll();
	ActivityDTO findOne(Integer id) throws IOException;
//11	Activity createFromFile(String fileName);

}
