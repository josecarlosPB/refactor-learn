package com.uca.refactor2.service;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.uca.refactor2.model.sonar.Analyse;
import com.uca.refactor2.model.sonar.Measure;
import com.uca.refactor2.model.sonar.MeasureOld;
import com.uca.refactor2.model.sonar.MeasureResponse;
import com.uca.refactor2.model.sonar.ProjectSearchResponse;
import com.uca.refactor2.model.sonar.issue.IssuesResponse;

@Service
public class SonarqubeServiceImpl implements SonarqubeService {

	private final String PROJECT_SEARCH_URL = "http://localhost:9000/api/project_analyses/search?project=refactor2";
	private final String MEASURE_CODE = "http://localhost:9000/api/measures/component?component=refactor2:activities/";
	private final String SONAR_URL = "http://localhost:9000/";
	private final String PROJECT_KEY = "refactor2";
	private final String SRC_FOLDER = "activities";
//	private final String SRC_FOLDER = "src/main/java/com/uca/refactor2/activities";
	private String EXECUTE_SONAR;
	private String GET_ISSUES_URL = SONAR_URL + "api/issues/search?componentKeys=refactor2:activities/";

	//private String GET_ISSUES_URL = SONAR_URL + "api/issues/search?componentKeys=refactor2:src/main/java/com/uca/refactor2/activities/";
	// http://localhost:9000/api/issues/search?componentKeys=refactor2:src/main/java/com/uca/refactor2/activities/DGCEPTMUCD21ZCAP.java&rules=squid:S2757
	private static String POM_PATH = "pom.xml";
	private RestTemplate restTemplate = new RestTemplate();
	private InvocationRequest request;
	private Invoker invoker;

	@PostConstruct
	private void init() {
		System.out.println("PostConstruct");
		buildSonarCommand();
		configureMavenWithSonar();
	}

	@Override
	public void runAnalysis() throws MavenInvocationException {
		// Assuming everything is set up (done in init)
		
		System.out.println("runAnalysys");
		invoker.execute(request);
	}
	
	@Override
	public IssuesResponse getIssuesFromFileName(String fileName, String ruleSearchName) {
		System.out.println("getIssuesFromFileName");
		String URL = GET_ISSUES_URL + fileName +"&rules="+ruleSearchName;
		System.out.println(URL);
		return restTemplate.getForObject(URL, IssuesResponse.class);
	}
	@Override
	public String getNumberOfCodeLine(String fileName) {
		String url = MEASURE_CODE+fileName+"&metricKeys=ncloc";
		System.out.println(url);
		MeasureResponse mr = restTemplate.getForObject(url, MeasureResponse.class);
		Collection<Measure> measure = mr.component.measures;
		Iterator<Measure> iterator = measure.iterator();
		return iterator.next().value;
	}
	//NOT USED
	@Override
	public void setTags(String url) {
		restTemplate.postForObject("http://localhost:9000/api/authentication/login?login=admin&password=admin", "", String.class);
		restTemplate.postForObject(url,"",String.class);
	}
	
	@Override
	public ProjectSearchResponse getProjectSearch() {
		return restTemplate.getForObject(PROJECT_SEARCH_URL, ProjectSearchResponse.class);
	}
//OLD
	/*
	@Override
	public IssuesResponse getIssuesFromFileName(String fileName) {
		System.out.println("getIssuesFromFileName");
		String URL = GET_ISSUES_URL + fileName;
		System.out.println(URL);
		return restTemplate.getForObject(URL, IssuesResponse.class);
	}
*/
	private void configureMavenWithSonar() {
		System.out.println("configure");
		request = new DefaultInvocationRequest();
		request.setPomFile(new File(POM_PATH));
		request.setGoals(Collections.singletonList(EXECUTE_SONAR));

		invoker = new DefaultInvoker();
		// Set maven home in case env variables are not set
		// (and using own installation)
		invoker.setMavenHome(new File("apache-maven-3.5.2"));
	}

	private void buildSonarCommand() {
		System.out.println("build");
		StringBuilder builder = new StringBuilder();
		builder.append("sonar:sonar ");
		builder.append(String.format("-Dsonar.host.url=%s ", SONAR_URL));
		builder.append(String.format("-Dsonar.projectKey=%s ", PROJECT_KEY));
		builder.append(String.format("-Dsonar.sources=%s ", SRC_FOLDER));
		builder.append(String.format("-Dsonar.login=%s ", "admin"));
		builder.append(String.format("-Dsonar.password=%s ", "admin"));
		EXECUTE_SONAR = builder.toString();
	}
}
