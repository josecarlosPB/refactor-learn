package com.uca.refactor2.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.HintDAO;
import com.uca.refactor2.model.Hint;

@Service
public class HintServiceImpl implements HintService{

	@Autowired
	private HintDAO hintDao;

	private static final Logger log = LoggerFactory.getLogger(HintServiceImpl.class);

	@Override
	public Hint findOne(Integer id) {
		log.info("Find hint with id = " + id);
		return hintDao.findOne(id);
	}

	@Override
	public List<Hint> findAll() {
		log.info("Find all hints");
		return hintDao.findAll();
	}

	@Override
	public Hint save(Hint hint) {
		if (hint.getId() == null) {
			// Not persisted yet
		} else {
			log.info("Update hint with id = " + hint.getId());
		}
		return hintDao.save(hint);
	}

}
