package com.uca.refactor2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.uca.refactor2.model.UserType;


@Entity
@Table(name="USER")
public class User implements Serializable {
	
	private static final long serialVersionUID = 4402583037980335445L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	@Column(unique = true, nullable = false)
	private String email;
	private String firstName;
	private String lastName;
    @Enumerated(EnumType.STRING) 
    @Column(nullable=false)
    private UserType  userType;
    
    private Integer activitiesCompleted;
    private Integer activitiesCompletedWithoutHints;
    private Integer activitiesWithoutHintsInARow;
    private Integer activitiesFirstTryInARow;
    private Integer activitiesPerfectInARow;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="joinedUserAchievementId.user")
    private List<JoinedUserAchievement> joinedUserAchievementList = new ArrayList<JoinedUserAchievement>();
	
    @OneToMany(cascade=CascadeType.ALL, mappedBy="joinedUserActivityId.user")
    private List<JoinedUserActivity> joinedUserActivityList = new ArrayList<JoinedUserActivity>();
	
    public User() {}
    
    public User(Integer id) {
        this.id = id;
    }
    public User(String email, String firstName, String lastName,
    		 UserType userType) {
    	this.email = email;
    	this.firstName = firstName;
    	this.lastName = lastName;
    	this.userType = userType;
    }
    

	public void setUserType(UserType userType) {
    	this.userType = userType;
    }
    
    public List<JoinedUserAchievement> getAllAchievement() {
    		return joinedUserAchievementList;
    }
    
    public List<JoinedUserActivity> getActivities() {
    	return joinedUserActivityList;
    }
    public void addAchievement(Achievement achievement) {
        // Notice a JoinedUserAchievement object
        Date dateOfAcquisition = new Date();
        JoinedUserAchievement joinedUserAchievement = new JoinedUserAchievement(new JoinedUserAchievement.JoinedUserAchievementId(this, achievement),dateOfAcquisition );
        joinedUserAchievement.setAchievement(achievement);
        joinedUserAchievementList.add(joinedUserAchievement);
    }
    //probably this is going to change a lot.
    public void addActivity(Activity activity, String fileName) {
        // Notice a JoinedUserAchievement object
        JoinedUserActivity joinedUserActivity = new JoinedUserActivity(new JoinedUserActivity.JoinedUserActivityId(this, activity), fileName);
        joinedUserActivity.setActivity(activity);
        joinedUserActivityList.add(joinedUserActivity);
    }
    
    public UserType getUserType() { 
        return userType;
    }
    
    public Integer getId() {
    	return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getActivitiesCompleted() {
		return activitiesCompleted;
	}

	public void setActivitiesCompleted(Integer activitiesCompleted) {
		this.activitiesCompleted = activitiesCompleted;
	}

	public Integer getActivitiesCompletedWithoutHints() {
		return activitiesCompletedWithoutHints;
	}

	public void setActivitiesCompletedWithoutHints(Integer activitiesCompletedWithoutHints) {
		this.activitiesCompletedWithoutHints = activitiesCompletedWithoutHints;
	}

	public Integer getActivitiesWithoutHintsInARow() {
		return activitiesWithoutHintsInARow;
	}

	public void setActivitiesWithoutHintsInARow(Integer activitiesWithoutHintsInARow) {
		this.activitiesWithoutHintsInARow = activitiesWithoutHintsInARow;
	}

	public Integer getActivitiesFirstTryInARow() {
		return activitiesFirstTryInARow;
	}

	public void setActivitiesFirstTryInARow(Integer activitiesFirstTryInARow) {
		this.activitiesFirstTryInARow = activitiesFirstTryInARow;
	}

	public Integer getActivitiesPerfectInARow() {
		return activitiesPerfectInARow;
	}

	public void setActivitiesPerfectInARow(Integer activitiesPerfectInARow) {
		this.activitiesPerfectInARow = activitiesPerfectInARow;
	}

    
}
