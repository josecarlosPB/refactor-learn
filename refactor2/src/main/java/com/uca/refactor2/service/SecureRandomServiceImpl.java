package com.uca.refactor2.service;

import java.security.SecureRandom;

import org.springframework.stereotype.Service;

@Service
public class SecureRandomServiceImpl implements SecureRandomService {

    private static final String symbols = "ABCDEFGJKLMNPRSTUVWXYZ0123456789";

    private SecureRandom random = new SecureRandom();

    private static final int defaultLength = 16;

    private char[] buf = new char[defaultLength];

    @Override
    public String randomAlphanumericalString() {
	for (int idx = 0; idx < buf.length; ++idx)
	    buf[idx] = symbols.charAt(random.nextInt(symbols.length()));
	return new String(buf);
    }
}
