package com.uca.refactor2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;




@Entity
@Table(name="ACHIEVEMENT")
public class Achievement implements Serializable{
	
	private static final long serialVersionUID = 7747630789725422177L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable=false)
	private String name;
    @Column(nullable=false)
	private Integer points;
    @Column(nullable=false)
	private String description;

	public Achievement() {
		
	}
	public Achievement(String name, Integer points, String description) {
		this.name = name;
		this.points = points;
		this.description=description;
	}

	public Achievement(Integer id) {
        this.id = id;
    }
	public String getName() {
		return name;
	}
	
	public Integer getId() {
    	return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	    
}
