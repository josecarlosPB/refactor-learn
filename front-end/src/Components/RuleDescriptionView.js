import React, {Component} from 'react';
import {PageHeader,Col, ButtonToolbar, DropdownButton, MenuItem, Modal, Row} from 'react-bootstrap';
import {withRouter} from 'react-router-dom';
import {Editor, EditorState, ContentState} from 'draft-js';
import DraftPasteProcessor from '../../node_modules/draft-js/lib/DraftPasteProcessor';
import withAuthorization from './withAuthorization';
import AuthUserContext from './AuthUserContext';
class RuleDescriptionViewShow extends Component {
    constructor(props) {
        super(props);
        
        const processedHTML = DraftPasteProcessor.processHTML(this.props.rule.description.replace(/\n/g, "<br />"));
        const contentState = ContentState.createFromBlockArray(processedHTML); 
        var editorState = EditorState.createWithContent(contentState);
        var editorState = EditorState.moveFocusToEnd(editorState);
        this.onChange = (editorState) => this.setState({editorState});
        this.state = {
            editorState: editorState,
            userId: null,
            userActivitiesId: [],
            userActivities: null,
            show: false
        };

  }

  componentDidMount() {
    
    var url = 'http://localhost:8080/users/search/' + this.props.email+ '/';
    fetch(url, {
    method: 'GET',
    headers: {
    'content-type': 'application/json'
    },  
    }).then(response => { return response.json();
    }).then(data => {
        this.setState({userId:data.id});
        console.log(data);
        var url2 = 'http://localhost:8080/users/' + this.state.userId + '/getActivitiesOfRule/' + this.props.match.params.id;
        fetch(url2, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
     },  
        }).then(response => { return response.json();
        }).then(data => {
            var userActivitiesId = []
            console.log(data);
            data.map((activity) =>  {
                userActivitiesId.push(activity.activity.id);
            }
        );
            this.setState({userActivitiesId:userActivitiesId, userActivities:data});
        });//second etch
    });//first fetch
   } 

   handleOnClick(activityId){
    this.setState({show:true});
    fetch('http://localhost:8080/users/'+this.state.userId+'/addActivity/'+activityId, {
        method: 'POST',
        headers: {
        'content-type': 'application/json'
      },  
    }).then(response => { return response.json();
    }).then(data => {
      console.log(data)
      this.setState({show:false});
      this.props.history.push({pathname:this.props.location.pathname+'/create/'+activityId, state: {userId:this.state.userId}})
    });
   }
   handleOnClickStarted(activityId){
    this.props.history.push({pathname:this.props.location.pathname+'/create/'+activityId, state: {userId:this.state.userId}})
   }
    render(){
        if(this.state.userActivities!==null) {
        var rule = this.props.rule.description;
        var nonStartedActivities = [];
        this.props.rule.activities.map((activity, index) => {
        if(!this.state.userActivitiesId.includes(activity.id)) { //if the activity is NOT started
            nonStartedActivities.push(activity);
        }
    }
 );

    var unStartedActivities = nonStartedActivities.map((activity, index) => {
     return <MenuItem eventKey={index}  onClick={() => this.handleOnClick(activity.id)}> {activity.name + " Difficulty: " + activity.difficulty}</MenuItem>  
         }
    );

    var startedActivitiesAndCompleted = [];
    var startedActivitiesNotCompleted = [];
    this.state.userActivities.map((activity, index) => {
        if(activity.score==10){
            startedActivitiesAndCompleted.push(activity);
        }
        else {
            startedActivitiesNotCompleted.push(activity);
        }

    });



    var startedActivitiesAndCompleted = startedActivitiesAndCompleted.map((activity, index) => {
        return <MenuItem eventKey={index}  onClick={() => this.handleOnClickStarted(activity.activity.id)}> {activity.activity.name + " Difficulty: " + activity.activity.difficulty}</MenuItem>  
            }
       );
    
    
    var startedActivitiesAndNotCompleted = startedActivitiesNotCompleted.map((activity, index) => {
        return <MenuItem eventKey={index}  onClick={() => this.handleOnClickStarted(activity.activity.id)}> {activity.activity.name + " Difficulty: " + activity.activity.difficulty}</MenuItem>  
            }
       );
       //we now have to filter between the ones with score 10 and the ones with less score so we can do another dropddown with completed activities
        return(
        <div>
        <Col smOffset={2} mdOffset={1}>
        <PageHeader>
            {this.props.rule.title}
        </PageHeader>
        </Col>
        
        
        <Col smOffset={2} mdOffset={1}>    
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}
          readOnly ={true}
        />
        </Col>

           
           <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>Add activity</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Please wait, the activity is being created
            </Modal.Body>
          </Modal>
        <Row>
        <Col smOffset={2} sm={10} md={2} mdOffset={1}>
        <ButtonToolbar>
            <DropdownButton title="Unstarted activities" id="dropdown-size-medium">
                {unStartedActivities}
            </DropdownButton>
        </ButtonToolbar>
        </Col>
        <Col sm={10} md={2} >
         <ButtonToolbar>
            <DropdownButton title="Started activities" id="dropdown-size-medium">
                {startedActivitiesAndNotCompleted}
            </DropdownButton>
        </ButtonToolbar>
        </Col>
        <Col sm={10} md={2} >        
        <ButtonToolbar>
            <DropdownButton title="Completed activities" id="dropdown-size-medium">
                {startedActivitiesAndCompleted}
            </DropdownButton>
        </ButtonToolbar>
        </Col>
        </Row>
         </div>
        );
        }
    else {
        return <div></div>;
    }
    }
}


const RuleDescriptionView = (props) => (
    <AuthUserContext.Consumer>
      {authUser =>
        <RuleDescriptionViewShow email={authUser.email} {...props}/>
      }
    </AuthUserContext.Consumer>
  )
  
const authCondition = (authUser) => !!authUser;
export default withAuthorization(authCondition)(withRouter(RuleDescriptionView));