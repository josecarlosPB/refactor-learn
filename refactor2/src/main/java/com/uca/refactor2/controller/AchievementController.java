package com.uca.refactor2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uca.refactor2.model.Achievement;
import com.uca.refactor2.service.AchievementService;

@RestController
@CrossOrigin
@RequestMapping(value = "/achievements")
public class AchievementController {
	
	@Autowired
	private AchievementService achievementService;
	
	@RequestMapping(value = "/{achievementId}", method = RequestMethod.GET)
	private Achievement findOne(@PathVariable("achievementId") Integer achievementId) {
		return achievementService.findOne(achievementId);
	}

	@RequestMapping(method = RequestMethod.GET)
	private List<Achievement> findAll() {
		return achievementService.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	private Achievement create(@RequestBody Achievement achievement) {
		achievement.setId(null); // To ensure we create instead of update
		return achievementService.save(achievement);
	}
	
	@RequestMapping(value = "/{achievementId}", method = RequestMethod.PUT)
	private Achievement update(@PathVariable("achievementId") Integer achievementId, @RequestBody Achievement achievement) {
		achievement.setId(achievementId); // To ensure we update instead of create
		return achievementService.save(achievement);
	}
	
	@RequestMapping(value = "/{achievementId}", method = RequestMethod.DELETE)
	private void delete(@PathVariable("achievementId") Integer achievementId) {
		final Achievement achievement = achievementService.findOne(achievementId);
		achievementService.delete(achievement);
	}

	
	
}
