import React, {Component} from 'react';
import {Form, FormGroup,FormControl,Col,Button, ControlLabel,PageHeader} from 'react-bootstrap';
import {withRouter} from 'react-router-dom';
import * as routes from '../constants/routes';
import { auth } from '../firebase';
import {SignUpLink} from './SignUp'
import { PasswordForgetLink } from './PasswordForget';

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null
  };

  const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
  

class SignIn extends Component {
    constructor(props, context) {
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { ...INITIAL_STATE };
    }
    handleSubmit(event) {
        const {
            email,
            password,
          } = this.state;
          
          auth.doSignInWithEmailAndPassword(email, password)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
            this.props.history.push(routes.HOME);
          })
          .catch(error => {
            this.setState(byPropKey('error', error));
          });
    
        event.preventDefault();
        
    }

    
    render()  {
        const {
            email,
            password,
            error,
          } = this.state;
      
          const isInvalid =
            password === '' ||
            email === '';
      
        return(         
            <div>
            
            <Col smOffset={2} mdOffset={1}>    
            <PageHeader>
             Log In
            </PageHeader>
            </Col>
             <Form horizontal onSubmit = {this.handleSubmit}>
                <FormGroup onChange = {event => this.setState(byPropKey('email', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Email
                    </Col>
                    <Col sm={6}>
                    <FormControl type="email" placeholder="email" />
                    </Col>
                </FormGroup>

                <FormGroup onChange = {event => this.setState(byPropKey('password', event.target.value))} controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                    Password
                    </Col>
                    <Col sm={6}>
                    <FormControl type="password" placeholder="Password" />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Sign In</Button>
                    </Col>
                </FormGroup>
            </Form>
            <PasswordForgetLink/>
            <SignUpLink />
            { error && <p>{error.message}</p> }
            </div>
        );
    }
    
}


export default withRouter(SignIn);
