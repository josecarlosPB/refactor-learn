import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import ActivityAdmin from './ActivityAdmin.js';
class ActivitiesListAdmin extends Component{
	render() {
        console.log(this.props.activities);
		var activities = this.props.activities.map((activity, index) =>
			<ActivityAdmin key ={index}  activity={activity}/>
    );
		return (
			<Table responsive>
                <tbody>
					<tr>
						<th>Activity name  </th>
					</tr>
					{activities}
				</tbody>
			</Table>
		);
	}
}
export default ActivitiesListAdmin;