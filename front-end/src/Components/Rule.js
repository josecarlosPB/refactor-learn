import React, {Component} from 'react';
import  {Link} from 'react-router-dom';
class Rule extends Component{

	render() {
        console.log(this.props.rule);
		return (
                <tr>
                    <td><Link to={"/rules/"+this.props.rule.id}>{this.props.rule.title}</Link></td>
                    <td>{this.props.rule.category}</td>
                </tr>
		);
	}
}

export default Rule;