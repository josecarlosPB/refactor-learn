import React, {Component} from 'react';
import AchievementList from './AchivementList';
class ListAchievements extends Component {
    constructor(props) {
		super(props);
		this.state = {achievements: []};
	}

	componentDidMount()  {
        var route = 'http://localhost:8080/users/'+this.props.match.params.id+'/achievements';
        fetch(route, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({achievements:data});
      console.log(data);
    });
	}


    render() {
        return(
        <AchievementList achievements = {this.state.achievements}/>);
        }
    }

export default ListAchievements;