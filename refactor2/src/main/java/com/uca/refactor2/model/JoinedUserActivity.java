
package com.uca.refactor2.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;




@Entity
@Table(name="USER_ACTIVITY")
public class JoinedUserActivity {

    public JoinedUserActivity() {}

    public JoinedUserActivity(JoinedUserActivityId joinedUserActivityId, String fileName) {
        this.joinedUserActivityId = joinedUserActivityId;
        this.fileName  = fileName;
        this.score = 0;
        this.lastHint = 0;
        this.numberOfTries=0;
    }
    @Column(nullable=false)
    private String fileName; //user activity filename
    @ManyToOne
    @JoinColumn(name="ACTIVITY_ID", insertable=false, updatable=false)
    private Activity activity;
    
    private Integer numberOfTries;
    private float score;
    private Integer lastHint; 
    //perfect means no tries and no hints at all
    public Activity getActivity() {
    	return activity;
    }
    
    public void setActivity(Activity activity) {
    	this.activity = activity;
    }

    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getLastHint() {
		return lastHint;
	}

	public void setLastHint(Integer lastHint) {
		this.lastHint = lastHint;
	}
	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}
	public Integer getNumberOfTries() {
		return numberOfTries;
	}

	public void setNumberOfTries(Integer numberOfTries) {
		this.numberOfTries = numberOfTries;
	}

	@EmbeddedId
    private JoinedUserActivityId joinedUserActivityId;
    
    
    // required because JoinedUserAchievments contains composite id
    @Embeddable
    public static class JoinedUserActivityId implements Serializable {

        /**
		 * 
		 */
		private static final long serialVersionUID = -9180674903145773104L;
		
		
		@ManyToOne
        @JoinColumn(name="USER_ID")
        private User user;
		
        @ManyToOne
        @JoinColumn(name="ACTIVITY_ID")
        private Activity activity;
        
        // required no arg constructor
        public JoinedUserActivityId() {}

        public JoinedUserActivityId(User user, Activity activity) {
            this.user = user;
            this.activity = activity;
        }

        public JoinedUserActivityId(Integer userId, Integer activityId) {
            this(new User(userId), new Activity(activityId));
        }

        public User getUser() {
        	return user;
        }

        
        public Activity getActivity() {
        	return activity;
        }
        
        public void setUser(User user) {
        	this.user = user;
        }
        
        public void setActivity(Activity activity) {
        	this.activity = activity;
        }
        @Override
        
        public boolean equals(Object instance) {
            if (instance == null)
                return false;

            if (!(instance instanceof JoinedUserActivityId))
                return false;

            final JoinedUserActivityId other = (JoinedUserActivityId) instance;
            if (!(user.getId().equals(other.getUser().getId())))
                return false;

            if (!(activity.getId().equals(other.getActivity().getId())))
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 47 * hash + (this.user != null ? this.user.hashCode() : 0);
            hash = 47 * hash + (this.activity != null ? this.activity.hashCode() : 0);
            return hash;
        }
    }
   }
    