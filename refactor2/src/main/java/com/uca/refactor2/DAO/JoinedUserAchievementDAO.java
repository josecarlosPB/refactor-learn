package com.uca.refactor2.DAO;





import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.JoinedUserAchievement;

import com.uca.refactor2.model.JoinedUserAchievement.JoinedUserAchievementId;



@Repository
public interface JoinedUserAchievementDAO extends CrudRepository<JoinedUserAchievement, JoinedUserAchievementId> {

	@Override
    JoinedUserAchievement findOne(JoinedUserAchievementId id);
}
