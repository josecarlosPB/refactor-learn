import React, {Component} from 'react';
import UserList from  './UserList.js';
class ListAllUsers extends Component {
    constructor(props) {
		super(props);
		this.state = {users: []};
	}

	componentDidMount() {

        fetch('http://localhost:8080/users', {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({users:data});
    });
	}

	render() {
		return (
			<UserList users = {this.state.users} />
		);
	}
}


export default ListAllUsers;