import React, {Component} from 'react';
import AceEditor from 'react-ace';
import {Row, Col} from 'react-bootstrap'
import {Table, Form, FormGroup,FormControl,Button, ControlLabel} from 'react-bootstrap';
import 'brace/mode/java';
import 'brace/theme/github';
import 'brace/theme/monokai';

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
  
class EditActivity extends Component {
    constructor(props) {
		super(props);
        this.state = {
            code: "Enter the code here",
            error: null,
            automaticHints: [],
            manualHints: [],
            newHint: ''
          };

           this.handleSubmit = this.handleSubmit.bind(this);
	}
    handleSubmit(event) {

    }
    //here we fetch the activity by id
	componentDidMount()  {
        var route = 'http://localhost:8080/activity/' + this.props.match.params.activityId;
        fetch(route, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({code:data.code});
      console.log(data);
    });
	}


    render() {
            
        return(
            <div>
            <Row>
              <Col md={12}>
              <AceEditor
                    ref={instance => { this.aceEditor = instance; }}
                    mode="java"
                    theme="monokai"
                    name="blah2"
                    onChange={this.handleCodeChange}
                    fontSize={14}
                    width='1500px'
                    showPrintMargin={false}
                    showGutter={true}
                    highlightActiveLine={true}
                    value={this.state.code}
                    setOptions={{
                        enableBasicAutocompletion: false,
                        enableLiveAutocompletion: false,
                        enableSnippets: false,
                        showLineNumbers: true,
                        tabSize: 2,
                }}/>
              </Col>
            </Row>
        </div>);
        }
    }

export default EditActivity;