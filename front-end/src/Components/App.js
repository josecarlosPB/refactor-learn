import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Navigation from './Navigation';
import Home from './Home';
import SignUp from './SignUp';
import SignIn from './SignIn';
import ListAllUsers from './ListAllUsers';
import ListAchievements from './ListAchievements';
import ListRules from './ListRules';
import RuleDescription from './RuleDescription';
import * as routes from '../constants/routes';
import Account from './Account';
import PasswordForget from './PasswordForget';
import Activity from './Activity';
import ListRulesAdmin from '../AdminComponents/ListRulesAdmin';
import RuleDescriptionAdmin from '../AdminComponents/RuleDescriptionAdmin';
import AddActivity from '../AdminComponents/AddActivity';
import withAuthentication from './withAuthentication';
import ListActivitiesAdmin from '../AdminComponents/ListActivitiesAdmin';
import EditActivity from '../AdminComponents/EditActivity';
import DeleteRuleAdmin from '../AdminComponents/DeleteRuleAdmin';
import NewTeacherAdmin from '../AdminComponents/NewTeacherAdmin';
import DeleteUserAdmin from '../AdminComponents/DeleteUserAdmin';
import CreateActivity from './CreateActivity';
import AddRule from '../AdminComponents/AddRule';
import Ranking from './Ranking';
import LandingI from './Landing';
class App extends Component {

  
  render() {
    return (
          <Router>
          <div>
          <Navigation/>
          <Route exact path={routes.LANDING} component={Home}/>
            <Route exact path={routes.SIGN_UP} component={SignUp}/>
            <Route exact path={routes.SIGN_IN} component={SignIn}/>
            <Route exact path={routes.ACCOUNT} component={Account}/>
            <Route exact path={routes.PASSWORD_FORGET} component={PasswordForget}/>
            <Route exact path={routes.ALL_RULES} component={ListRules}/>
            <Route exact path={routes.SPECIFIC_RULE} render={props => <RuleDescription {... props} />} />
            <Route exact path={routes.EDIT_RULE} render={props => <RuleDescriptionAdmin {... props} />} />
            <Route exact path={routes.ALL_USERS} component = {ListAllUsers}/>
            <Route path={routes.USER_ACHIEVEMENTS} render={props => <ListAchievements {...props} /> } />
            <Route exact path="/Activity" component={Activity}/>
            <Route exact path={routes.ADMIN_RULES} component={ListRulesAdmin}/>
            <Route exact path={routes.ADD_ACTIVITY} render={props => <AddActivity {...props} /> } />
            <Route exact path={routes.LIST_ACTIVITIES_BY_RULE} render={props => <ListActivitiesAdmin {...props} /> } />
            <Route exact path={routes.EDIT_ACTIVITY} render={props => <EditActivity {...props} /> } />
            <Route exact path={routes.DELETE_RULE} render={props => <DeleteRuleAdmin {...props} /> } />
            <Route exact path={routes.NEW_TEACHER} render={props => <NewTeacherAdmin {...props} /> } />
            <Route exact path={routes.DELETE_USER} render={props => <DeleteUserAdmin {...props} /> } />
            <Route exact path={routes.CREATE_USER_ACTIVITY} render={props => <CreateActivity {...props} /> } />
            <Route exact path={routes.NEW_RULE} render={props => <AddRule {...props} /> } />
            <Route exact path={routes.RANKING} render={props => <Ranking {...props} /> } />
            <Route exact path={routes.HOME} render={props => <LandingI {...props} /> } />

          </div>
          </Router>

    );
  }
}

export default withAuthentication(App);
