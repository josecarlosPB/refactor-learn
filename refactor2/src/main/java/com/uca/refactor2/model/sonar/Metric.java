package com.uca.refactor2.model.sonar;

public class Metric {

    public String key;

    public String name;

    public String description;

    public String domain;

    public String type;

    public Boolean higherValuesAreBetter;

    public Boolean qualitative;

    public Boolean hidden;

    public Boolean custom;

    @Override
    public String toString() {
	return "Metric [key=" + key + ", name=" + name + ", description=" + description + ", domain=" + domain
		+ ", type=" + type + ", higherValuesAreBetter=" + higherValuesAreBetter + ", qualitative=" + qualitative
		+ ", hidden=" + hidden + ", custom=" + custom + "]";
    }
}
