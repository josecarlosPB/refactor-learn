package com.uca.refactor2.DAO;
import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.User;
import com.uca.refactor2.model.UserType;

@Repository
public interface UserDAO extends CrudRepository<User, Integer> {

	User findOne(Integer id);

	List<User> findAll();

	User save(User user);

	void delete(User user);

	User findByEmail(String email);
    List<User> findByUserType(UserType userType);
	void deleteAllInBatch();
}
