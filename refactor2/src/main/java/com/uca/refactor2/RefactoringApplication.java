package com.uca.refactor2;

import java.io.FileInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.uca.refactor2.model.Achievement;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.User;
import com.uca.refactor2.model.UserType;
import com.uca.refactor2.service.AchievementService;
import com.uca.refactor2.service.FileService;
import com.uca.refactor2.service.RuleService;
import com.uca.refactor2.service.UserService;

@SpringBootApplication
public class RefactoringApplication implements CommandLineRunner {
@Autowired
UserService userService;

@Autowired
AchievementService achievementService;

@Autowired
RuleService ruleService;

@Autowired
FileService fileService;

	public static void main(String[] args) {

		SpringApplication.run(RefactoringApplication.class, args);
	}
	@Override
	public void run(String... arg0) throws Exception {
		//iitialize firease
		//TODO Change this
		FileInputStream serviceAccount = new FileInputStream("C:\\Users\\carli\\eclipse-workspace\\refactor2\\src\\main\\java\\com\\uca\\refactor2\\serviceAccountKey.json");
		//String filePath = "activities/P11KEVLL3TPWEXJJ.java";
		//fileService.deleteFile(filePath);
		FirebaseOptions options = new FirebaseOptions.Builder()
		    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
		    .setDatabaseUrl("https://<DATABASE_NAME>.firebaseio.com/")
		    .build();

		FirebaseApp.initializeApp(options);
		if(userService.getUserByType(UserType.ADMIN).size()==0) {
			CreateRequest request = new CreateRequest()
				    .setEmail("refactoringuca@gmail.com")
				    .setPassword("refactoradmin")
				    .setDisplayName("admin");
				UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
				System.out.println("Successfully created new user: " + userRecord.getUid());
		        userService.createAdminUser(userRecord.getUid());
		}
/*
		

		String titler1 = "Magic number should not be used";
		String category = "Code Smell";
		String descriptionr1 = "A magic number is a number that comes out of nowhere, and is directly used in a statement. Magic numbers are often used, for instance to limit the number of iterations of a loops, to test the value of a property, etc.\n" + 
				"\n" + 
				"Using magic numbers may seem obvious and straightforward when you're writing a piece of code, but they are much less obvious and straightforward at debugging time.\n" + 
				"\n" + 
				"That is why magic numbers must be demystified by first being assigned to clearly named variables before being used.\n" + 
				"\n" + 
				"-1, 0 and 1 are not considered magic numbers.\n" + 
				"Noncompliant Code Example\n" + 
				"\n" + 
				"public static void doSomething() {\n" + 
				"	for(int i = 0; i < 4; i++){                 // Noncompliant, 4 is a magic number\n" + 
				"		...\n" + 
				"	}\n" + 
				"}\n" + 
				"\n" + 
				"Compliant Solution\n" + 
				"\n" + 
				"public static final int NUMBER_OF_CYCLES = 4;\n" + 
				"public static void doSomething() {\n" + 
				"  for(int i = 0; i < NUMBER_OF_CYCLES ; i++){\n" + 
				"    ...\n" + 
				"  }\n" + 
				"}\n" + 
				"\n" + 
				"Exceptions\n" + 
				"\n" + 
				"This rule ignores hashCode methods.\n";*/
		/*
		String titler2 =  "=+ should not be used instead of +=";
		String category2 = "Bug";
		String descriptionr2 = "The use of operators pairs ( =+, =- or =! ) where the reversed, single operator was meant (+=, -= or !=) will compile and run, but not produce the expected results.\n" + 
				"\n" + 
				"This rule raises an issue when =+, =-, or =! is used without any spacing between the two operators and when there is at least one whitespace character after.\n" + 
				"Noncompliant Code Example\n" + 
				"\n" + 
				"int target = -5;\n" + 
				"int num = 3;\n" + 
				"\n" + 
				"target =- num;  // Noncompliant; target = -3. Is that really what's meant?\n" + 
				"target =+ num; // Noncompliant; target = 3\n" + 
				"\n" + 
				"Compliant Solution\n" + 
				"\n" + 
				"int target = -5;\n" + 
				"int num = 3;\n" + 
				"\n" + 
				"target = -num;  // Compliant; intent to assign inverse value of num is clear\n" + 
				"target += num;\n";
		
*/
//		Rule rule = new Rule(titler1,category,descriptionr1, "squid:S109");
	//	Rule rule2 = new Rule(titler2,category2,descriptionr2,"squid:S2757");
	//	ruleService.save(rule);
//		ruleService.save(rule2);
		
		String title="Your refactoring road starts here!";
		String description= "First activity completed";
		Integer points= 10;
		String title2="Apprentice refactor";
		String description2= "10 Activities completed";
		Integer points2= 30;
		String title3="Master of refactoring!";
		String description3= "50 Activities completed";
		Integer points3= 70;
		String title4="First steps without help";
		String description4= "First activity completed without hints";
		Integer points4= 10;
		String title5="You don't like hints!";
		String description5= "10 Activities without hints completed";
		Integer points5= 30;
		String title6="Hints are not needed anymore";
		String description6= "50 Activities without hints completed";
		Integer points6= 70;
		String title7="Did you forgot that hints exists?";
		String description7= "3 Activities in a row without hints";
		Integer points7= 20;
		String title8="You don't need tries";
		String description8= "3 Activities in a row finished at first try";
		Integer points8= 20;
		String title9="Perfectionist";
		String description9= "3 Activities in a row finished without hints and at first try";
		Integer points9= 40;
		Achievement ach1= new Achievement(title,points,description);
		Achievement ach2= new Achievement(title2,points2,description2);
		Achievement ach3= new Achievement(title3,points3,description3);
		Achievement ach4= new Achievement(title4,points4,description4);
		Achievement ach5= new Achievement(title5,points5,description5);
		Achievement ach6= new Achievement(title6,points6,description6);
		Achievement ach7= new Achievement(title7,points7,description7);
		Achievement ach8= new Achievement(title8,points8,description8);
		Achievement ach9= new Achievement(title9,points9,description9);
		achievementService.save(ach1);
		achievementService.save(ach2);
		achievementService.save(ach3);
		achievementService.save(ach4);
		achievementService.save(ach5);
		achievementService.save(ach6);
		achievementService.save(ach7);
		achievementService.save(ach8);
		achievementService.save(ach9);
				
	}
}
