import React, {Component} from 'react';
import  {Link} from 'react-router-dom';
class CountItem extends Component{

	render() {

		return (
                <tr>
                    <td><Link to={"/rules/"+ this.props.count[1]}>{this.props.count[0]}</Link></td>
                    <td>You have {this.props.count[2]} activities on this rule</td>
                    <td><Link to={"/rules/"+ this.props.count[1]}>View activities statistics</Link></td>

                </tr>
		);
	}
}

export default CountItem;