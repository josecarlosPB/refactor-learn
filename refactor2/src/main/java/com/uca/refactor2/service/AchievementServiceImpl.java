package com.uca.refactor2.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.AchievementDAO;
import com.uca.refactor2.model.Achievement;

@Service
public class AchievementServiceImpl implements AchievementService{

	@Autowired
	private AchievementDAO achievementDao;

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public Achievement findOne(Integer id) {
		log.info("Find achievement with id = " + id);
		return achievementDao.findOne(id);
	}

	@Override
	public List<Achievement> findAll() {
		log.info("Find all achievements");
		return achievementDao.findAll();
	}

	@Override
	public Achievement save(Achievement achievement) {
		if (achievement.getId() == null) {
			// Not persisted yet
			log.info("Create achievement with name = " + achievement.getName());
		} else {
			log.info("Update achievement with id = " + achievement.getId());
		}
		return achievementDao.save(achievement);
	}

	@Override
	public void delete(Achievement achievement) {
		log.info("Delete achievement with id = " + achievement.getId());
		achievementDao.delete(achievement);
	}

	@Override
	public Achievement findAchievementByName(String name) {
		log.info("Find achievement with name = " + name);
		return achievementDao.findAchievementByName(name);
	}

}
