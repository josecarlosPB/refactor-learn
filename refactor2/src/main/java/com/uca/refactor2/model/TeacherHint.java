package com.uca.refactor2.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
	@Entity
	@Table(name="TEACHER_HINT")
	public class TeacherHint implements Serializable {

		private static final long serialVersionUID = 1949624285056576983L;
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
		Integer id;
	    @Column(nullable=false)
		String hint;
		
		public TeacherHint() {}
		public TeacherHint(String hint) {
			this.hint = hint;
		}
		
		public Integer getId() {
			return id;
		}
		
		public void setId(Integer id) {
			this.id = id;
		}
		
		public String getHint() {
			return hint;
		}
		public void setHint(String hint) {
			this.hint = hint;
		}
	
}
