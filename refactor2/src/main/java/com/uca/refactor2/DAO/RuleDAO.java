package com.uca.refactor2.DAO;


import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.uca.refactor2.model.Rule;

@Repository
public interface RuleDAO extends CrudRepository<Rule, Integer>  {
	
	Rule findOne(Integer id);
	
	List<Rule> findAll();
	
	Rule save (Rule Rule);
	
	void delete(Rule rule);
	
	Rule findRuleByTitle(String title);
	
	void deleteAllInBatch();
	
	
}