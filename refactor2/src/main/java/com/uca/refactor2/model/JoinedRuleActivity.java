package com.uca.refactor2.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/*
@Entity
@Table(name="RULE_ACTIVITY")
*/
public class JoinedRuleActivity {
	  public JoinedRuleActivity() {}

	    public JoinedRuleActivity(JoinedRuleActivityId joinedRuleActivityId) {
	        this.joinedRuleActivityId = joinedRuleActivityId;
	    }
	    
	    @ManyToOne
	    @JoinColumn(name="ACTIVITY_ID", insertable=false, updatable=false)
	    private Activity activity;
	    	    	 
	    public Activity getActivity() {
	    	return activity;
	    }
	    
	    public void setActivity(Activity activity) {
	    	this.activity = activity;
	    }

	    @EmbeddedId
	    private JoinedRuleActivityId joinedRuleActivityId;
	    
	    
	    @Embeddable
	    public static class JoinedRuleActivityId implements Serializable {

			private static final long serialVersionUID = 2015726292052854803L;

	
			
			
			@ManyToOne
	        @JoinColumn(name="RULE_ID")
	        private Rule rule;
			
	        @ManyToOne
	        @JoinColumn(name="ACTIVITY_ID")
	        private Activity activity;
	        
	        // required no arg constructor
	        public JoinedRuleActivityId() {}

	        public JoinedRuleActivityId(Rule rule, Activity activity) {
	            this.rule = rule;
	            this.activity = activity;
	        }

	        public JoinedRuleActivityId(Integer ruleId, Integer activityId) {
	            this(new Rule(ruleId), new Activity(activityId));
	        }

	        public Rule getRule() {
	        	return rule;
	        }

	        
	        public Activity getActivity() {
	        	return activity;
	        }
	        
	        public void setRule(Rule rule) {
	        	this.rule = rule;
	        }
	        
	        public void setActivity(Activity activity) {
	        	this.activity = activity;
	        }
	        
	        /*
	        @Override
	        public boolean equals(Object instance) {
	            if (instance == null)
	                return false;

	            if (!(instance instanceof JoinedRuleActivityId))
	                return false;

	            final JoinedRuleActivityId other = (JoinedRuleActivityId) instance;
	            if (!(rule.getId().equals(other.getRule().getId())))
	                return false;

	            if (!(activity.getId().equals(other.getActivity().getId())))
	                return false;

	            return true;
	        }

	        @Override
	        public int hashCode() {
	            int hash = 7;
	            hash = 47 * hash + (this.rule != null ? this.rule.hashCode() : 0);
	            hash = 47 * hash + (this.activity != null ? this.activity.hashCode() : 0);
	            return hash;
	        }*/
	    }

	   }
	    
