package com.uca.refactor2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.uca.refactor2.exception.NotCompilableException;
import com.uca.refactor2.exception.NotCompilableOrZeroIssuesException;
import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.model.JoinedUserAchievement;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.User;
import com.uca.refactor2.model.UserType;
import com.uca.refactor2.service.ActivityService;
import com.uca.refactor2.service.RuleService;
import com.uca.refactor2.service.UserService;

import java.io.IOException;
import java.util.List;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import junit.framework.TestCase;
import org.springframework.dao.*;

/**
 * Unit test for simple App.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
@Autowired
UserService userService;
@Autowired
RuleService ruleService;
@Autowired
ActivityService activityService;

private static boolean initialized = false;

	@Before
	public void initializeData() throws MavenInvocationException, InterruptedException, NotCompilableOrZeroIssuesException  {
		if(initialized==false) {
	    String email = "testEmail@gmail.com";
        String firstName = "firstNameTest";
        String lastName = "lastNameTest";
        UserType userType = UserType.STUDENT;
        User user = new User(email,firstName,lastName,userType);
        userService.save(user);
        String title =  "Magic number";
		String category = "Bug";
		String description = "test";
		String searchName = "squid:S109";
		Rule rule = new Rule(title,category,description,searchName);
		Rule createdRule = ruleService.save(rule); //we've created the rule needed to test
		String code ="package com.uca.refactor2.activities;\r\n" + 
				"\r\n" + 
				"public class Ejercicio {\r\n" + 
				"	 public static void main(String[] args) {\r\n" + 
				"	 int[] anArray;\r\n" + 
				"	 anArray = new int[10];\r\n" + 
				"	 // initialize first element\r\n" + 
				"     anArray[0] = 100;\r\n" + 
				"     // initialize second element\r\n" + 
				"     anArray[1] = 200;\r\n" + 
				"     // and so forth\r\n" + 
				"     anArray[2] = 300;\r\n" + 
				"     anArray[3] = 400;\r\n" + 
				"     anArray[4] = 500;\r\n" + 
				"     anArray[5] = 600;\r\n" + 
				"     anArray[6] = 700;\r\n" + 
				"     anArray[7] = 800;\r\n" + 
				"     anArray[8] = 900;\r\n" + 
				"     anArray[9] = 1000;\r\n" + 
				"     int i;\r\n" + 
				"     for(i=0; i<=9;i++) {\r\n" + 
				"    	 System.out.println(anArray[i]);\r\n" + 
				"     }\r\n" + 
				"	 }\r\n" + 
				"}\r\n";
		ActivityDTO activity = new ActivityDTO();
		activity.setCode(code);
		activity.setDifficulty(1);
		activity.setName("TestActivity");
		activityService.addActivity(activity, createdRule.getId()); //This add the activity to the rule, but this should give NotCompilableException	
		initialized = true;
		}
	}

	@Test(expected = DataIntegrityViolationException.class)
    public void testUserNeedsAllData()
    {
        User user = new User();
        //Both userType and email are required values
        String email = "testEmail2@gmail.com";
        String firstName = "firstNameTest";
        String lastName = "lastNameTest";
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        userService.save(user);

       
    }
	@Test(expected = NotCompilableOrZeroIssuesException.class)
	public void addNotCompilableActivity() throws MavenInvocationException, InterruptedException, NotCompilableOrZeroIssuesException {
		ActivityDTO activity = new ActivityDTO();
		activity.setCode("this code won't work!");
        Integer ruleId = 1 ; //we're going to use the previous rule added 
		activity.setDifficulty(1);
		activity.setName("TestActivity");
		activityService.addActivity(activity, ruleId); //This add the activity to the rule, but this should give NotCompilableException	
	}

	@Test
	public void testAddActivity() throws MavenInvocationException, InterruptedException, NotCompilableOrZeroIssuesException
	{
		String code = "package com.uca.refactor2.activities;\r\n" + 
				"\r\n" + 
				"public class Ejercicio {\r\n" + 
				"	 public static void main(String[] args) {\r\n" + 
				"	 int[] anArray;\r\n" + 
				"	 anArray = new int[10];\r\n" + 
				"	 // initialize first element\r\n" + 
				"     anArray[0] = 100;\r\n" + 
				"     // initialize second element\r\n" + 
				"     anArray[1] = 200;\r\n" + 
				"     // and so forth\r\n" + 
				"     anArray[2] = 300;\r\n" + 
				"     anArray[3] = 400;\r\n" + 
				"     anArray[4] = 500;\r\n" + 
				"     anArray[5] = 600;\r\n" + 
				"     anArray[6] = 700;\r\n" + 
				"     anArray[7] = 800;\r\n" + 
				"     anArray[8] = 900;\r\n" + 
				"     anArray[9] = 1000;\r\n" + 
				"     int i;\r\n" + 
				"     for(i=0; i<=9;i++) {\r\n" + 
				"    	 System.out.println(anArray[i]);\r\n" + 
				"     }\r\n" + 
				"	 }\r\n" + 
				"}\r\n";
		
		ActivityDTO activityDTO = new ActivityDTO();
		activityDTO.setDifficulty(5);
		activityDTO.setName("testActivity");
		activityDTO.setCode(code);
		Integer expectedNumberOfIssues = 20;
		Integer ruleId = 1; //because we created one before all tests
		int expectedNumberOfHints = 41; //this should be always number of issues*2  +1
		Activity createdActivity = activityService.addActivity(activityDTO, ruleId); //now we check if the activity has been created correctly
		assertEquals(createdActivity.getName(),activityDTO.getName());
		assertEquals(createdActivity.getNumberOfIssues(),expectedNumberOfIssues); //this activity has 2 issues of that rule
		assertEquals(createdActivity.getHints().size(),expectedNumberOfHints);
		
	}
	@Test
	public void testAnalyzeActivity() throws IOException, InterruptedException, MavenInvocationException, NotCompilableException
	{
		//we're going to test the good case where we solve the exercise created before running the tests
		Integer userId = 1;
		Integer activityId = 1;
		userService.addActivity(userId,activityId);
		//Now we have the relationship between  user and activity, we're gonna solve it and analyze.
		ActivityDTO activity = userService.getUserActivity(userId, activityId);
		//we change the code to the right one
		activity.setCode("package com.uca.refactor2.activities;\r\n" + 
				"\r\n" + 
				"public class Ejercicio {\r\n" + 
				"	 public static void main(String[] args) {\r\n" + 
				"	 int[] anArray;\r\n" + 
				"	 	 int arraySize = 10;\r\n" + 
				"	 anArray = new int[arraySize];\r\n" + 
				"	 // initialize first element\r\n" + 
				"\r\n" + 
				"	  for(int i=0; i<=arraySize-1;i++) {\r\n" + 
				"     anArray[i] = i*100;\r\n" + 
				"}\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"     for(int i=0; i<=arraySize-1;i++) {\r\n" + 
				"    	 System.out.println(anArray[i]);\r\n" + 
				"     }\r\n" + 
				"	 }\r\n" + 
				"}\r\n");
		activity.setRuleId(1);
		ActivityDTO resultActivity = userService.analyzeActivity(userId, activityId, activity);
		//now we can check that we got a perfect score
		float expectedScore = 10;
		assertEquals(expectedScore,resultActivity.getScore());
		//we also have unlock 3 new achievements, first activity completed, first activity completed without hints and first activity at first try
		List<JoinedUserAchievement> achievements = userService.findOne(1).getAllAchievement();
		int expectedAchievementNumber = 3;
		assertEquals(achievements.size(),expectedAchievementNumber);
	}
}


