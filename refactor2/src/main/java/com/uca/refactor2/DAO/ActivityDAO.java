package com.uca.refactor2.DAO;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.Activity;
@Repository
public interface ActivityDAO extends CrudRepository<Activity, Integer> {

    @Override
    Activity findOne(Integer id);

    @Override
    Collection<Activity> findAll();
    
    @Override
    Activity save(Activity activity);
    
}
