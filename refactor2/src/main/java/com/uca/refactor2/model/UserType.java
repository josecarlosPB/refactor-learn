package com.uca.refactor2.model;


public enum UserType {
	   STUDENT, TEACHER, ADMIN
	}