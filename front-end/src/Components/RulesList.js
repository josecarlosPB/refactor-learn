import React, {Component} from 'react';
import {Table, Button} from 'react-bootstrap';
import Rule from './Rule';
import SearchInput, {createFilter} from 'react-search-input'
import '../react-search-input.css'; 

class RulesList extends Component{

    constructor (props) {
        super(props)
        this.state = {
          searchTerm: ''
        }
        this.searchUpdated = this.searchUpdated.bind(this)
      }
 
      
	render() {
        const KEYS_TO_FILTERS = ['title', 'category'];
        const filteredRules = this.props.rules.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

        console.log(filteredRules);
		var rules = filteredRules.map((rule, index) =>
			<Rule key ={index}  rule={rule}/>
    );
		return (
            <div>
            <SearchInput className="search-input" onChange={this.searchUpdated} />
			<Table responsive>
                <tbody>
					<tr>
						<th>Rule name  </th>
						<th>Rule category</th>
					</tr>
					{rules}
				</tbody>
			</Table>
            </div>
		);
    }
    
    searchUpdated (term) {
        this.setState({searchTerm: term})
      }
}
export default RulesList;