package com.uca.refactor2.DAO;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uca.refactor2.model.TeacherHint;

@Repository
public interface TeacherHintDAO extends CrudRepository<TeacherHint, Integer>{
		
			
	TeacherHint findOne(Integer id);
				
	TeacherHint save (TeacherHint teacherHint);
	List<TeacherHint> findAll();	
	void delete(TeacherHint teacherHint);
	}



