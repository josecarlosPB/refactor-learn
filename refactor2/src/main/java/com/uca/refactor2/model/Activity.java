package com.uca.refactor2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Activity implements Serializable {

	private static final long serialVersionUID = 4964894839753333457L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
    @Column(nullable=false)
	private Integer numberOfIssues;
    @Column(nullable=false)
	private String fileName;
    @Column(nullable=false)
	private String name;
    @Column(nullable=false)
	private float difficulty;
	@OneToMany
	@JoinColumn(name="HINT_ID")
	private List<Hint> hints = new ArrayList<Hint>();
	
	@JsonIgnore
	@ManyToOne
	Rule rule;
	
	public Rule getRule() {
		return rule;
	}
	
	public void setRule(Rule rule) {
		this.rule = rule;
	}
	public Activity() {
		super();
	}
	
	public Activity(Integer id) {
		this.id = id;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public List<Hint> getHints() {
		return hints;
	}
	public void addHint(Hint hint) {
		hints.add(hint);
	}


	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Activity [id=" + id + ", fileName=" + fileName +  ","
				+ "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public Integer getNumberOfIssues() {
		return numberOfIssues;
	}

	public void setNumberOfIssues(Integer numberOfIssues) {
		this.numberOfIssues = numberOfIssues;
	}

	public float getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(float difficulty) {
		this.difficulty = difficulty;
	}




}
