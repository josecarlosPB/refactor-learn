package com.uca.refactor2.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.ActivityDAO;
import com.uca.refactor2.exception.NotCompilableOrZeroIssuesException;
import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.model.Hint;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.sonar.issue.Issue;
import com.uca.refactor2.model.sonar.issue.IssuesResponse;



@Service
public class ActivityServiceImpl implements ActivityService {
	private final String activitiesPath = "activities/";
	//private final String activitiesPath = "src/main/java/com/uca/refactor2/activities/";
//	private final String activitiesPath = "C:\\Users\\carli\\eclipse-workspace\\refactor2\\src\\main\\java\\com\\uca\\refactor2\\activities";

	@Autowired
	private SecureRandomService randomService;

	@Autowired
	private FileService fileService;

	@Autowired
	private SonarqubeService sonarqubeService;

	@Autowired
	private RuleService ruleService;
	
	@Autowired
	private ActivityDAO activityDao;
	
	@Autowired
	private HintService hintService;
	

	
	@Override
	public Activity findOneActivity(Integer id) {
		return activityDao.findOne(id);
	}
	@Override
	public ActivityDTO findOne(Integer id) throws IOException {
		Activity a = activityDao.findOne(id);
		ActivityDTO act = new ActivityDTO();
		//TODO Change this
		String path = "activities/" + a.getFileName(); 
		String code = fileService.readFile(path);
		act.setCode(code);
		act.setHints(a.getHints());
		act.setActivityId(a.getId());
		act.setRuleId(a.getRule().getId());
		return act;
	}
	@Override 
	public Activity addActivity(ActivityDTO activityDTO, Integer ruleId) throws MavenInvocationException, InterruptedException, NotCompilableOrZeroIssuesException {
		//first of all we'll build the new activity
		System.out.println(ruleId);
		Activity activity = new Activity();
		activity.setFileName(randomService.randomAlphanumericalString() + ".java");
		try {
			fileService.createFile(activitiesPath, activity.getFileName(), activityDTO.getCode());
		} catch (IOException e) {
			e.printStackTrace();
		}
		//we don't need here to set code activity to null because our code is on activityDTO
		//we now should get the hints with sonarqube
		sonarqubeService.runAnalysis(); //we analyze the new activity
		//now we have to get the issues of the activity
		//we should filter here only the rule that we need, we can do it thanks to rule id
		//TimeUnit.SECONDS.sleep(6); //Needed to give sonarqube time to refresh 
		Rule rule = ruleService.findOne(ruleId);
		Collection<Issue> issues = sonarqubeService.getIssuesFromFileName(activity.getFileName(), rule.getSearchName()).issues ;

		int cont = 0;
		while (issues.size() == 0 && cont <=10) {
			 issues = sonarqubeService.getIssuesFromFileName(activity.getFileName(), rule.getSearchName()).issues ;
			 TimeUnit.SECONDS.sleep(6);
			 cont  = cont+1;
			 System.out.println("we are here on while loop");
		}
		 issues = sonarqubeService.getIssuesFromFileName(activity.getFileName(), rule.getSearchName()).issues ;
		if(issues.size()==0) {
			fileService.deleteFile(activitiesPath + activity.getFileName());
			System.out.println("Ops, something happened, the activity has not been created");
			throw new NotCompilableOrZeroIssuesException();
		}
		Iterator<Issue> iterator = issues.iterator();
		//first hint is the number of problems
		Integer size = issues.size();
		activity.setNumberOfIssues(size);
		Hint issueNumber = new Hint("There are " + size + " issues to be fixed");
		hintService.save(issueNumber);
		activity.addHint(issueNumber);
		Random rand = new Random();

	        while (iterator.hasNext()) {
	    		Integer  n = (Integer) rand.nextInt(8) + 1;
	    		System.out.println(n);
	    		Integer issueLine = iterator.next().line;
	    		System.out.println(issueLine);
	    		Integer minNumber = issueLine - n;
	    		System.out.println(minNumber);
	        	if(minNumber < 1) {
	        		Integer initialLine =1;
		        	Integer hintLine =  (Integer) Math.round(issueLine/initialLine);
		        	String aroundLineHintstr = "One Issue is around the line " + Integer.toString(hintLine);
		        	System.out.println(aroundLineHintstr);
		        	Hint aroundLineHint = new Hint(aroundLineHintstr);
		        	hintService.save(aroundLineHint);
		    		activity.addHint(aroundLineHint);
	        	}
	        	else {
	        		Integer initialLine = issueLine  - n;
		        	Integer hintLine = (Integer) Math.round(issueLine/initialLine);
		        	String aroundLineHintstr = "One Issue is around the line " + Integer.toString(hintLine);
		        	Hint aroundLineHint = new Hint(aroundLineHintstr);
		        	hintService.save(aroundLineHint);
		    		activity.addHint(aroundLineHint);
	        	}
	        	String issueExactLine = "One issue is on the line " + Integer.toString(issueLine);
	        	Hint exactLineHint = new Hint(issueExactLine);
	        	hintService.save(exactLineHint);
	    		activity.addHint(exactLineHint);
 }
		//now we should add the new activity to the rule
	    activity.setRule(rule);
	    activity.setName(activityDTO.getName());
	    activity.setDifficulty(activityDTO.getDifficulty());
	    activityDao.save(activity);
		rule.addActivity(activity); //activity added to the rule4
		ruleService.save(rule);
		return activity;
	}
	/*
	@Override
	public Activity create(Activity activity) {

	
		// Validate data

		// Avoiding name-generated file names prevents injection
		activity.setFileName(randomService.randomAlphanumericalString() + ".java");

		try {
			fileService.createFile(activitiesPath, activity.getFileName(), activity.getCode());
		} catch (IOException e) {
			e.printStackTrace();
		}
		// No sonarqube interaction yet
		activity.setCode(null); // TODO improve this
		return activityDao.save(activity);
	}
*/
	/**
	 * Only works with activities that are in the correct folder
	 * 
	 * @param fileName
	 */
	/*
	@Override
	public Activity createFromFile(String fileName) {
		String filePath = activitiesPath + "/" + fileName;
		String code = null;
		try {
			code = fileService.readFile(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Activity activity = new Activity();
		activity.setFileName(fileName);
		activity.setCode(null); // TODO improve this
		return activityDao.save(activity);

	}
*/
	/**
	 * Takes an existing activity and performs an analysis on it. Currently
	 * returning the issues instead of persisting them.
	 */
	/*
	@Override
	public Collection<Issue> analyze(Activity activity) {
		return sonarqubeService.getIssuesFromFileName(activity.getFileName()).issues;
	}
*/
	/*
	@Override
	public Collection<Issue> analyze(Integer id) {
		return analyze(activityDao.findOne(id));
	}
*/
	@Override
	public Collection<Activity> findAll() {
		return activityDao.findAll();
	}
}
