import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import Achievement from './Achievement';
class AchievementList extends Component{
	render() {
        console.log(this.props.achievements);
		var achievements = this.props.achievements.map((achievement, index) =>
			<Achievement key ={index}  achievement={achievement}/>
    );
		return (
			<Table responsive>
                <tbody>
					<tr>
						<th>Achievement name</th>
						<th>Description</th>
						<th>Points</th>
                        <th>Date of acquisition</th>
					</tr>
					{achievements}
				</tbody>
			</Table>
		);
	}
}
export default AchievementList;