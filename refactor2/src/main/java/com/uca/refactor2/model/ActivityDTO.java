package com.uca.refactor2.model;

import java.io.Serializable;
import java.util.List;

//NOT USED
//OLD VERSION
public class ActivityDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 775277746998637196L;
	private String code;
	private Integer ruleId;
	private List<Hint> hints;
	private float difficulty;
	private Integer activityId;
	private String fileName;
	private String name;
	private Integer lastHint;
	private float score;
	private Integer numberOfIssues;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getRuleId() {
		return ruleId;
	}
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}
	public List<Hint> getHints() {
		return hints;
	}
	public void setHints(List<Hint> hints) {
		this.hints = hints;
	}
	public Integer getActivityId() {
		return activityId;
	}
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}


	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getLastHint() {
		return lastHint;
	}
	public void setLastHint(Integer lastHint) {
		this.lastHint = lastHint;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public Integer getNumberOfIssues() {
		return numberOfIssues;
	}
	public void setNumberOfIssues(Integer numberOfIssues) {
		this.numberOfIssues = numberOfIssues;
	}
	public float getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(float difficulty) {
		this.difficulty = difficulty;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
