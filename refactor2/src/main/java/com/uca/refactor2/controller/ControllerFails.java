package com.uca.refactor2.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
import com.uca.refactor2.exception.NotCompilableException;
import com.uca.refactor2.exception.NotCompilableOrZeroIssuesException;
import com.uca.refactor2.model.ApiErrorDTO;

@ControllerAdvice ( basePackages = { "com.uca.refactor2.controller" } )
public class ControllerFails {
	@ResponseBody
	@ExceptionHandler(NotCompilableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiErrorDTO error(NotCompilableException e){
		return new ApiErrorDTO(400, e.getMessage());
	}
	@ResponseBody
	@ExceptionHandler(NotCompilableOrZeroIssuesException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiErrorDTO error(NotCompilableOrZeroIssuesException e){
		return new ApiErrorDTO(400, e.getMessage());
	}
}
