package com.uca.refactor2.model.sonar.issue;

import com.uca.refactor2.model.sonar.Paging;
import java.util.Collection;

public class IssuesResponse {

	public Paging paging;

	public Collection<Issue> issues;

	// Skipped some params
}
