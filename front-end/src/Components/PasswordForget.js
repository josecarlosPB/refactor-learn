import React, {Component} from 'react';
import {Form, FormGroup,FormControl,Col,Button, ControlLabel,PageHeader} from 'react-bootstrap';
import {withRouter, Link} from 'react-router-dom';
import * as routes from '../constants/routes';
import { auth } from '../firebase';

const INITIAL_STATE = {
    email: '',
    error: null
  };

  const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
  });
  

class PasswordForget extends Component {
    constructor(props, context) {
        super(props,context);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { ...INITIAL_STATE };
    }
    handleSubmit(event) {
        const {
            email,
          } = this.state;
          
          auth.doPasswordReset(email)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
          })
          .catch(error => {
            this.setState(byPropKey('error', error));
          });
    
        event.preventDefault();
        
    }

    
    render()  {
        const {
            email,
            error,
          } = this.state;
      
          const isInvalid = email === '';
      
        return(         
            <div>
            
            <Col smOffset={2} mdOffset={1}>    
            <PageHeader>
             Reset password
            </PageHeader>
            </Col>
             <Form horizontal onSubmit = {this.handleSubmit}>
                <FormGroup onChange = {event => this.setState(byPropKey('email', event.target.value))} controlId="formHorizontalUsername">
                    <Col componentClass={ControlLabel} sm={2}>
                    Email
                    </Col>
                    <Col sm={6}>
                    <FormControl type="email" placeholder="email" />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col smOffset={2} sm={6}>
                    <Button disabled={isInvalid}  type="submit">Reset My Password</Button>
                    </Col>
                </FormGroup>
            </Form>
            { error && <p>{error.message}</p> }
            </div>
        );
    }
    
}

const PasswordForgetLink = () =>
<Col smOffset={2}>
    <Link to={routes.PASSWORD_FORGET}>Forgot Password?</Link>
 </Col>


export {
  PasswordForgetLink,
};
export default withRouter(PasswordForget);
