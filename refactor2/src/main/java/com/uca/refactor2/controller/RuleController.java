package com.uca.refactor2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.JoinedRuleActivity;
import com.uca.refactor2.model.Rule;
import com.uca.refactor2.model.RuleDTO;
import com.uca.refactor2.service.RuleService;

@RestController
@CrossOrigin
@RequestMapping(value = "/rules")
public class RuleController {
	
	@Autowired
	private RuleService ruleService;
	
	@RequestMapping(value = "/{ruleId}", method = RequestMethod.GET)
	private Rule findOne(@PathVariable("ruleId") Integer ruleId) {
		return ruleService.findOne(ruleId);
	}
	
	@RequestMapping(value = "/{ruleId}/activities", method = RequestMethod.GET)
	private List<Activity>  findActivities(@PathVariable("ruleId") Integer ruleId) {
		System.out.println("rule/activities");
		return ruleService.findActivities(ruleId);
	}
	@RequestMapping(method = RequestMethod.GET)
	private List<Rule> findAll() {
		return ruleService.findAll();
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	private Rule create(@RequestBody Rule rule) {
		rule.setId(null); // To ensure we create instead of update
		
		System.out.println("wtf is going here");
		return ruleService.save(rule);
	}
	
	@RequestMapping(value = "/{ruleId}", method = RequestMethod.PUT)
	private Rule update(@PathVariable("ruleId") Integer ruleId, @RequestBody RuleDTO ruleDTO) {
		//rule.setId(ruleId); // To ensure we update instead of create
		return ruleService.update(ruleDTO);
	}
	
	@RequestMapping(value = "/{ruleId}", method = RequestMethod.DELETE)
	private void delete(@PathVariable("ruleId") Integer ruleId) {
		final Rule rule = ruleService.findOne(ruleId);
		ruleService.delete(rule);
	}

	
	
}