package com.uca.refactor2.service;

import java.io.IOException;

public interface FileService {

    void createFile(String path, String name, String content) throws IOException;

    void deleteAllFilesExcept(String filePath, String dirPath);

    String readFile(String path) throws IOException;

	void deleteFile(String filePath);

}
