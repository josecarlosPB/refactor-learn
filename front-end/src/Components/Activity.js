import React, {Component} from 'react';
import fetch from 'isomorphic-fetch';
import AceEditor from 'react-ace';
import {Row, Col, Button} from 'react-bootstrap';
import 'brace/mode/java';
import 'brace/theme/github';
import 'brace/theme/monokai';


import Results from './Results';

export default class Activity extends Component {

  // Add context
  constructor(props, context) {
    super(props, context);
    this.state = {
      code: "Enter the code here",
      activityId: null,
      issues: [],
    };
    this.handleCodeChange = this.handleCodeChange.bind(this);
    this.handleStart = this.handleStart.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleAnalyze = this.handleAnalyze.bind(this);
  }

  render() {
    return (
      <div>
        <Row>
          <Col md={8}>
          <AceEditor
                ref={instance => { this.aceEditor = instance; }}
                mode="java"
                theme="monokai"
                name="blah2"
                onChange={this.handleCodeChange}
                fontSize={14}
                showPrintMargin={true}
                showGutter={true}
                highlightActiveLine={true}
                value={this.state.code}
                setOptions={{
                    enableBasicAutocompletion: false,
                    enableLiveAutocompletion: false,
                    enableSnippets: false,
                    showLineNumbers: true,
                    tabSize: 2,
            }}/>
          </Col>
          <Col md={4}>
            <Button onClick = { this.handleCreate }> Create Activity </Button>
            <Button onClick = { this.handleStart }> Start Sonar </Button>
            <Button onClick = { this.handleAnalyze }> Analyze </Button>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
          <Results issues={this.state.issues} />
          </Col>
        </Row>
      </div>
    );
  }

  handleCodeChange(newValue) {
    this.setState({
      code: newValue,
    });
  }

  handleStart() {
    fetch('http://localhost:8080/sonarqube/run', {
      method: 'GET',
    }).catch((response) => console.log(response));
  }

  handleCreate() {
    fetch('http://localhost:8080/activity', {
      method: 'POST',
      body: JSON.stringify({ "code": this.state.code }),
      headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
      console.log(data);
      this.setState({
        activityId: data.id,
      });
    });
  }

  handleAnalyze() {
    fetch('http://localhost:8080/activity/' + this.state.activityId + '/analyze', {
      method: 'GET',
    }).then(response => { return response.json();
    }).then(data => {
      console.log(data);
      this.setState({
        issues: data,
      });
    });

    }
}
