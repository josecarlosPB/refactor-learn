package com.uca.refactor2.controller;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.uca.refactor2.exception.NotCompilableException;
import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.model.JoinedUserAchievement;
import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.User;
import com.uca.refactor2.model.UserDTO;
import com.uca.refactor2.model.UserType;
import com.uca.refactor2.service.UserService;

@RestController
@CrossOrigin
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@CrossOrigin
	@RequestMapping(value = "/getAllStudents", method = RequestMethod.GET)
	private List<User> getAllStudents() {
		return userService.getAllStudents();
	}
	@CrossOrigin
	@RequestMapping(value = "/{userId}/newHint/{activityId}",method = RequestMethod.PUT)
	private void newHint(@PathVariable("userId") Integer userId, @PathVariable("activityId") Integer activityId)  {
		 userService.newHint(userId,activityId);
	}
	//TODO  maybe create a new JoinedUserActivit controller and change this functions
	@CrossOrigin
	@RequestMapping(value = "/{userId}/achievements", method = RequestMethod.GET)
	private List<JoinedUserAchievement> findAchievements(@PathVariable("userId") Integer userId) {
		return userService.getAchievements(userId);
	}

	@CrossOrigin
	@RequestMapping(value = "/{userId}/getActivitiesOfRule/{ruleId}", method = RequestMethod.GET)
	private List<JoinedUserActivity> getActivitiesOfRule(@PathVariable("userId") Integer userId, @PathVariable("ruleId") Integer ruleId){
		return userService.getActivitiesOfRule(userId,ruleId);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/{userId}/updateActivity/{activityId}", method = RequestMethod.PUT)
	private ActivityDTO updateActivity(@PathVariable("userId") Integer userId, @PathVariable("activityId") Integer activityId, @RequestBody ActivityDTO activityDTO){
		return userService.updateActivity(userId,activityId,activityDTO);
	}
	@CrossOrigin
	@RequestMapping(value = "/{userId}/getUserActivity/{activityId}", method = RequestMethod.GET)
	private ActivityDTO getUserActivity(@PathVariable("userId") Integer userId, @PathVariable("activityId") Integer activityId) throws IOException {
		return userService.getUserActivity(userId,activityId);
	}
	@CrossOrigin
	@RequestMapping(value = "/{userId}/analyze/{activityId}", method = RequestMethod.PUT)
	private ActivityDTO analyze(@PathVariable("userId") Integer userId, @PathVariable("activityId") Integer activityId, @RequestBody ActivityDTO activityDTO ) throws MavenInvocationException, IOException, InterruptedException, NotCompilableException {
		return userService.analyzeActivity(userId,activityId,activityDTO);
	}
	@CrossOrigin
	@RequestMapping(value = "/{userId}/addActivity/{activityId}", method = RequestMethod.POST)
	private ActivityDTO addActivity(@PathVariable("userId") Integer userId, @PathVariable("activityId") Integer activityId)   throws IOException, InterruptedException, MavenInvocationException{
		return userService.addActivity(userId,activityId);
	}
	
	//----------------------------------------------------
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	private User findOne(@PathVariable("userId") Integer userId) {
		return userService.findOne(userId);
	}
	
	//New method, this will have firebase custom to add teacher role to every new student. 
		@RequestMapping(value = "/teacher", method = RequestMethod.POST)
		private User createTeacher(@RequestBody UserDTO userDTO) {
			//Create the new User
			User user = new User();
			user.setUserType(UserType.TEACHER);
			user.setEmail(userDTO.getEmail());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setActivitiesCompleted(0);
			user.setActivitiesCompletedWithoutHints(0);
			user.setActivitiesFirstTryInARow(0);
			user.setActivitiesPerfectInARow(0);
			//verify the user and create custom role
			String idToken = userDTO.getIdToken();
	        FirebaseToken decoded;
			try {
				decoded = FirebaseAuth.getInstance().verifyIdToken(idToken);
		        Map<String, Object> claims = new HashMap<>();
		        claims.put("teacher", true);
		        String uid = decoded.getUid(); 
				FirebaseAuth.getInstance().setCustomUserClaims(uid, claims);
			} catch (FirebaseAuthException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

	        return userService.save(user);

		}
	//New method, this will have firebase custom to add student role to every new student. 
	@RequestMapping(value = "/student", method = RequestMethod.POST)
	private User createStudent(@RequestBody UserDTO userDTO) {
		//Create the new User
		User user = new User();
		user.setUserType(UserType.STUDENT);
		user.setEmail(userDTO.getEmail());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setActivitiesCompleted(0);
		user.setActivitiesCompletedWithoutHints(0);
		user.setActivitiesFirstTryInARow(0);
		user.setActivitiesPerfectInARow(0);
		user.setActivitiesWithoutHintsInARow(0);
		//verify the user and create custom role
		String idToken = userDTO.getIdToken();
        FirebaseToken decoded;
		try {
			decoded = FirebaseAuth.getInstance().verifyIdToken(idToken);
	        Map<String, Object> claims = new HashMap<>();
	        claims.put("student", true);
	        String uid = decoded.getUid(); 
			FirebaseAuth.getInstance().setCustomUserClaims(uid, claims);
		} catch (FirebaseAuthException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        return userService.save(user);

	}
	@RequestMapping(method = RequestMethod.GET)
	private List<User> findAll() {
		return userService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	private User create(@RequestBody User user) {
		user.setId(null); // To ensure we create instead of update
		return userService.save(user);
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
	private User update(@PathVariable("userId") Integer userId, @RequestBody User user) {
		user.setId(userId); // To ensure we update instead of create
		return userService.save(user);
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	private void delete(@PathVariable("userId") Integer userId) {
		final User user = userService.findOne(userId);
		userService.delete(user);
	}
	@CrossOrigin
	@RequestMapping(value = "/search/{email}", method = RequestMethod.GET)
	private User findUserByEmail(@PathVariable("email") String email) {
		return userService.findUserByEmail(email);
	}
}