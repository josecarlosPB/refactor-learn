package com.uca.refactor2.model.sonar.issue;

public class Comment {

	public String key;

	public String login;

	public String htmlText;

	public String markdown;

	public Boolean updatable;

	public String createdAt;
}
