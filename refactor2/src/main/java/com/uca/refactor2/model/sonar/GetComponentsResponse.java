package com.uca.refactor2.model.sonar;

import java.util.List;

public class GetComponentsResponse {

    public Paging paging;

    public List<SonarComponent> components;

    @Override
    public String toString() {
	return "GetComponentsResponse [paging=" + paging + ", components=" + components + "]";
    }

}
