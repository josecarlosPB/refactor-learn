import React, {Component} from 'react';
class Achievement extends Component{

	render() {
        console.log(this.props.achievement.achievement.name)
		return (
                <tr>
                    <td>{this.props.achievement.achievement.name}</td>
                    <td>{this.props.achievement.achievement.description}</td>
                    <td>{this.props.achievement.achievement.points}</td>
                    <td>{this.props.achievement.date}</td>
        

                </tr>
		);
	}
}

export default Achievement;