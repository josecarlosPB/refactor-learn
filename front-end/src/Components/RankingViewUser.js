import React, {Component} from 'react';
import  {Link} from 'react-router-dom';
class RankingViewUser extends Component{

	render() {
        console.log(this.props.user);
		return (
                <tr>
                    <td>{this.props.rank}</td>
                    <td>{this.props.user.firstName + " " + this.props.user.lastName}</td>
                    <td>{this.props.user.sumAchPoints}</td>
                    <td>{this.props.user.activitiesCompleted}</td>
                </tr>
		);
	}
}

export default RankingViewUser;
