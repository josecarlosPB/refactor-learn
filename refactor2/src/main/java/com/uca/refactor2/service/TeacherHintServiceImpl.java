package com.uca.refactor2.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uca.refactor2.DAO.TeacherHintDAO;
import com.uca.refactor2.model.TeacherHint;
@Service
public class TeacherHintServiceImpl implements TeacherHintService {

	@Autowired
	private TeacherHintDAO teacherHintDao;

	private static final Logger log = LoggerFactory.getLogger(HintServiceImpl.class);

	@Override
	public TeacherHint findOne(Integer id) {
		log.info("Find hint with id = " + id);
		return teacherHintDao.findOne(id);
	}

	@Override
	public TeacherHint save(TeacherHint teacherHint) {
		return teacherHintDao.save(teacherHint);
	}

	@Override
	public void delete(TeacherHint teacherHint) {
		teacherHintDao.delete(teacherHint);
		
	}

	@Override
	public List<TeacherHint> findAll() {
		return teacherHintDao.findAll();
	}

}


