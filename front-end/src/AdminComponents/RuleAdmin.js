import React, {Component} from 'react';
import  {Link} from 'react-router-dom';
class RuleAdmin extends Component{

	render() {
		return (
                <tr>
                    <td><Link to={"/rules/"+this.props.rule.id}>{this.props.rule.title}</Link></td>
                    <td>{this.props.rule.category}</td>
                    <td><Link to={"/rules/"+ this.props.rule.id + "/delete"}>Delete rule</Link></td>
                    <td><Link to={"/rules/"+ this.props.rule.id + "/edit"}>Edit rule</Link></td>
                    <td><Link to={"/rules/"+ this.props.rule.id + "/addActivity"}>Add activity</Link></td>
                    <td><Link to={"/rules/" + this.props.rule.id + "/activities"}>View activities</Link></td>
                    
                </tr>
		);
	}
}

export default RuleAdmin;