package com.uca.refactor2.model.sonar;

public class Event {
	public String key;
	public String category;
	public String name;
}
