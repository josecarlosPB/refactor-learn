package com.uca.refactor2.service;



import com.uca.refactor2.model.JoinedUserAchievement;
import com.uca.refactor2.model.JoinedUserAchievement.JoinedUserAchievementId;

public interface JoinedUserAchievementService {

    public JoinedUserAchievement findOne(JoinedUserAchievementId id);
	
}
