package com.uca.refactor2.model.sonar;
import java.util.Collection;

public class Analyse {
	public String key;
	public String date;
	public Collection<Event> events;
}
