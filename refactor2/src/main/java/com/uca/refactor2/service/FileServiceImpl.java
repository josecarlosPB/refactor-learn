package com.uca.refactor2.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl implements FileService {

    @Override
    public void createFile(String path, String name, String content) throws IOException {
	File file = new File(path + "/" + name);
	file.createNewFile();
	FileUtils.writeStringToFile(file, content);
    }

    // May change string to blob in the future
    @Override
    public String readFile(String path) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(path));
	String separator = System.lineSeparator();
	StringBuilder sb = new StringBuilder();
	String line = br.readLine();

	while (line != null) {
	    sb.append(line);
	    sb.append(separator);
	    line = br.readLine();
	}
	br.close();
	return sb.toString();
    }
    
    @Override
    public void deleteFile(String filePath) {
    	File file = new File(filePath);
    	file.delete();
    }
    @Override
    public void deleteAllFilesExcept(String filePath, String dirPath) {
	File dir = new File(dirPath);
	for (File file : dir.listFiles()) {
	    if (!file.getName().equals(filePath)) {
		file.delete();
	    }
	}
    }
}
