import React, {Component} from 'react';
import {Table, Button, Col} from 'react-bootstrap';
import RuleAdmin from './RuleAdmin';
import SearchInput, {createFilter} from 'react-search-input';
import {withRouter} from 'react-router-dom';
class RulesListAdmin extends Component{

	constructor (props) {
        super(props)
        this.state = {
          searchTerm: ''
        }
				this.searchUpdated = this.searchUpdated.bind(this);
				this.handleOnClick = this.handleOnClick.bind(this);
      }
 
			handleOnClick() {
				this.props.history.push({pathname:'/newRule'});
			}
	render() {
        const KEYS_TO_FILTERS = ['title', 'category'];
        const filteredRules = this.props.rules.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

        console.log(filteredRules);
		var rules = filteredRules.map((rule, index) =>
			<RuleAdmin key ={index}  rule={rule}/>
    );
		return (
			<div>
            <SearchInput className="search-input" onChange={this.searchUpdated} />
			<Table responsive>
                <tbody>
					<tr>
						<th>Rule name  </th>
						<th>Rule category</th>
						<th> 				    <Button onClick = {this.handleOnClick}> New Rule</Button></th>
					</tr>
					{rules}
				</tbody>
			</Table>
			</div>
		);
	}
	searchUpdated (term) {
        this.setState({searchTerm: term})
      }
}
export default withRouter(RulesListAdmin);