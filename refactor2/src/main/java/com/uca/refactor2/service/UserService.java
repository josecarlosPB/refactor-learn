package com.uca.refactor2.service;
import java.io.IOException;
import java.util.List;

import org.apache.maven.shared.invoker.MavenInvocationException;

import com.google.firebase.auth.FirebaseAuthException;
import com.uca.refactor2.exception.NotCompilableException;
import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.model.JoinedUserAchievement;
import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.User;
import com.uca.refactor2.model.UserType;


public interface UserService {

	User findOne(Integer id);
	
	List<JoinedUserAchievement> getAchievements(Integer id);

    List<User> findAll();

    User save(User user);
    ActivityDTO analyzeActivity(Integer userId, Integer activityId, ActivityDTO activityDTO) throws MavenInvocationException, IOException, InterruptedException, NotCompilableException;
    ActivityDTO addActivity(Integer userId, Integer activityId)  throws IOException, InterruptedException, MavenInvocationException;
    void delete(User user);
    
    User findUserByEmail(String email);

	ActivityDTO getUserActivity(Integer userId, Integer activityId) throws IOException;

	ActivityDTO updateActivity(Integer userId, Integer activityId, ActivityDTO activityDTO);

	List<JoinedUserActivity> getActivitiesOfRule(Integer userId, Integer ruleId);

	void newHint(Integer userId, Integer activityId);

	List<User> getAllStudents();

	List<User> getUserByType(UserType userType);

	void createAdminUser(String uid)throws FirebaseAuthException;

}
