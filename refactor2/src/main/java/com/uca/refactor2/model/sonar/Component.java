package com.uca.refactor2.model.sonar;

import java.util.Collection;

public class Component {
	public String id;
    public String key;
	public String name;
	public String qualifier;
	public String path;
	public String language;
	public Collection<Measure> measures;
}
