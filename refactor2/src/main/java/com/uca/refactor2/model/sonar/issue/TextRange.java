package com.uca.refactor2.model.sonar.issue;

public class TextRange {

	public Integer startLine;

	public Integer endLine;

	public Integer startOffset;

	public Integer endOffset;
}
