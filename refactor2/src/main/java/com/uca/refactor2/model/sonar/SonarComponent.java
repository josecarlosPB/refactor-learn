package com.uca.refactor2.model.sonar;

public class SonarComponent {

    public String organization;

    public String id;

    public String key;

    public String qualifier;

    public String name;

    public String project;

    public String language;

    @Override
    public String toString() {
	return "SonarComponent [organization=" + organization + ", id=" + id + ", key=" + key + ", qualifier="
		+ qualifier + ", name=" + name + ", project=" + project + ", language=" + language + "]";
    }

}
