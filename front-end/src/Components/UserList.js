import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import User from './User';
class UserList extends Component{
	render() {
        console.log(this.props.users);
		var users = this.props.users.map((user, index) =>
			<User key ={index}  user={user}/>
    );
		return (
			<Table responsive>
                <tbody>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
                        <th>User type</th>
						<th>Achievements</th>
					</tr>
					{users}
				</tbody>
			</Table>
		);
	}
}
export default UserList;