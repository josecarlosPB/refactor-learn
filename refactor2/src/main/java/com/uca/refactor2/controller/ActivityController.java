 package com.uca.refactor2.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uca.refactor2.model.ActivityDTO;
import com.uca.refactor2.exception.NotCompilableOrZeroIssuesException;
import com.uca.refactor2.model.Activity;
import com.uca.refactor2.model.JoinedUserActivity;
import com.uca.refactor2.model.sonar.issue.Issue;
import com.uca.refactor2.service.ActivityService;
import com.uca.refactor2.service.JoinedUserActivityService;


@RestController
@CrossOrigin
@RequestMapping(value = "activity")
public class ActivityController {
	@Autowired
	JoinedUserActivityService joinedUserActivityService;
	@Autowired
	private ActivityService activityService;
/*	
	@RequestMapping(method = RequestMethod.POST)
	private Activity create(@RequestBody ActivityDTO activity) {
		return activityService.create(activity);
	}
	*/
	@RequestMapping(value ="/count/{userId}", method = RequestMethod.GET)
	private List<JoinedUserActivity> countActivities(@PathVariable("userId")Integer userId) {
		return joinedUserActivityService.countActivities(userId);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	private ActivityDTO findOne(@PathVariable("id") Integer id) throws IOException {
		return activityService.findOne(id);
	}

	@RequestMapping(value = "/{id}/addActivity", method = RequestMethod.POST)
	private Activity addActivity(@RequestBody ActivityDTO activity, @PathVariable("id") Integer ruleId) throws MavenInvocationException, InterruptedException, NotCompilableOrZeroIssuesException {
		return activityService.addActivity(activity, ruleId);
	}
/*
	@RequestMapping(value = "/{id}/analyze", method = RequestMethod.GET)
	private Collection<Issue> analyze(@PathVariable("id") Integer id) {
		return activityService.analyze(id);
	}
*/
	@RequestMapping(method = RequestMethod.GET)
	private Collection<Activity> findAll() {
		return activityService.findAll();
	}

}
