import React, {Component} from 'react';
import  {Link} from 'react-router-dom';
class User extends Component{

	render() {

		return (
                <tr>
                    <td>{this.props.user.firstName}</td>
                    <td>{this.props.user.lastName}</td>
                    <td>{this.props.user.email}</td>
                    <td>{this.props.user.userType}</td>
                    <td><Link to={"/users/"+ this.props.user.id+ "/achievements"}>View achievements</Link></td>

                </tr>
		);
	}
}

export default User;
//<td><Link to={"/users/"+ this.props.user.id + "/edit"}>Edit user</Link></td>
//<td><Link to={"/users/"+ this.props.user.id + "/delete"}>Delete user</Link></td>