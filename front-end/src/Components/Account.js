import React, {Component} from 'react';

import AuthUserContext from './AuthUserContext';
import withAuthorization from './withAuthorization';
import PasswordChange from './PasswordChange';
class AccountShow extends Component {
  constructor(props) {
		super(props);
    this.state = {user: []};
  }
  
  
	componentDidMount() {
        console.log(this.props.authUser.email);
        
        var url = 'http://localhost:8080/users/search/' + this.props.authUser.email+ '/';
        console.log(url);
        fetch(url, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },  
    }).then(response => { return response.json();
    }).then(data => {
      this.setState({user:data});
      console.log(data);
    });
   } 


  
  render() {
  return(
  <AuthUserContext.Consumer>


    {authUser =>
      <div>
        <h1>Account: {authUser.email}</h1>
        <PasswordChange email = {authUser.email}/>
      </div>
    }



  
  </AuthUserContext.Consumer>);
    }
  }

  const Account = () => (
    <AuthUserContext.Consumer>
      {authUser =>
        <AccountShow authUser={authUser} />
      }
    </AuthUserContext.Consumer>
  )

            
const authCondition = (authUser) => !!authUser
const roleNeeded = "NONE";

export default withAuthorization(authCondition, roleNeeded)(Account);