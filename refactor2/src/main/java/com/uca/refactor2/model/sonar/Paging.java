package com.uca.refactor2.model.sonar;

public class Paging {

    public Integer pageIndex;

    public Integer pageSize;

    public Integer total;

    @Override
    public String toString() {
	return "Paging [pageIndex=" + pageIndex + ", pageSize=" + pageSize + ", total=" + total + "]";
    }

}
