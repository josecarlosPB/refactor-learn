package com.uca.refactor2.controller;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uca.refactor2.service.SonarqubeService;
@CrossOrigin
@RestController
@RequestMapping(value = "sonarqube")
public class SonarqubeController {

	@Autowired
	private SonarqubeService sonarqubeService;

	@RequestMapping(value = "/run")
	private ResponseEntity runAnalysis() {
		try {
			System.out.println("/Run/try");
			sonarqubeService.runAnalysis();
		} catch (MavenInvocationException e) {
			System.out.println("/Run/catch");
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		System.out.println("/Run/Ok");
		return new ResponseEntity(HttpStatus.OK);
	}
}
