import React, {Component} from 'react';
import RulesListAdmin from './RulesListAdmin';
import withAuthorization from '../Components/withAuthorization';

class ListRulesAdmin extends Component {
    constructor(props) {
		super(props);
		this.state = {rules: []};
	}

	componentDidMount()  {
        var route = 'http://localhost:8080/rules';
        fetch(route, {
        method: 'GET',
        headers: {
        'content-type': 'application/json'
      },
    }).then(response => { return response.json();
    }).then(data => {
        console.log(data);
        data.sort((a,b) => {
            return a.title > b.title;
        });
      this.setState({rules:data});
      console.log(data);
    });
	}


    render() {
        return(
        <RulesListAdmin rules = {this.state.rules}/>);

        }
    }

            
    const authCondition = (authUser) => !!authUser
    const roleNeeded = "TEACHER";
    
    export default withAuthorization(authCondition, roleNeeded)(ListRulesAdmin);