package com.uca.refactor2.model.sonar;
import com.uca.refactor2.model.sonar.Paging;
import java.util.Collection;
public class ProjectSearchResponse {

		public Paging paging;

		public Collection<Analyse> analyses;

		// Skipped some params
	}

