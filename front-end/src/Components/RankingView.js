import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import RankingViewUser from './RankingViewUser';
class RankingView extends Component{
	render() {
        var rank = 1;
        var lastValue;
        var users = this.props.users.map((user, index) => {
             if(index>0) {
                if(user.sumAchPoints!==lastValue){
                    rank = rank+1;
                    lastValue = user.sumAchPoints;
                }
             }
             else {
                 lastValue = user.sumAchPoints;
             }
			return <RankingViewUser key ={index}  user={user} rank={rank}/>
        });

		return (
			<Table responsive>
                <tbody>
					<tr>
                        <th>Rank</th>
						<th>User</th>
						<th>Achievement points</th>
						<th>Activities completed</th>
					</tr>
					{users}
				</tbody>
			</Table>
		);
	}
}
export default RankingView;