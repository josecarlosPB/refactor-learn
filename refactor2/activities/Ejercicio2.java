
public class Ejercicio2 {
	public int execute(final CoreBlock operatorBlock, final DAGVertex dagVertex) throws SDF4JException, WorkflowException {

	    final SDFVertex sdfVertex = dagVertex.getPropertyBean().getValue(DAGVertex.SDF_VERTEX, SDFVertex.class);

	    final Object refinement = sdfVertex.getPropertyBean().getValue(AbstractVertex.REFINEMENT);


	    if (refinement instanceof AbstractGraph) {


	      final SDFGraph graph = (SDFGraph) sdfVertex.getGraphDescription();

	      final List<SDFAbstractVertex> repVertexs = new ArrayList<>();

	      final List<SDFInterfaceVertex> interfaces = new ArrayList<>();

	      final IbsdfFlattener flattener = new IbsdfFlattener(graph, 10);

	      SDFGraph resultGraph = null;

	      try {

	        flattener.flattenGraph();

	        resultGraph = flattener.getFlattenedGraph();

	      } catch (final SDF4JException e) {

	        throw (new WorkflowException(e.getMessage(), e));

	      }

	      resultGraph.validateModel(WorkflowLogger.getLogger()); // compute repetition vectors

	      if (resultGraph.isSchedulable() == false) {

	        throw (new WorkflowException("HSDF Build Loops generate clustering: Graph not schedulable"));

	      }


	      for (final SDFAbstractVertex v : resultGraph.vertexSet()) {

	        if (v instanceof SDFVertex) {

	          repVertexs.add(v);

	        }



	        if (v instanceof SDFInterfaceVertex) {

	          interfaces.add((SDFInterfaceVertex) v);


	        }

	      }

	      final HSDFBuildLoops loopBuilder = new HSDFBuildLoops(this.scenario, null);

	      final AbstractClust clust = graph.getPropertyBean().getValue(MapperDAG.CLUSTERED_VERTEX, AbstractClust.class);

	      if (clust == null) {

	        throw (new WorkflowException("Loop Codegen failed. Please make sure the clustering workflow is run."));

	      }


	      final List<SDFAbstractVertex> inputRepVertexs = new ArrayList<>();

	      final List<SDFAbstractVertex> outputRepVertexs = new ArrayList<>();

	      for (final SDFInterfaceVertex i : interfaces) {


	        for (final SDFInterfaceVertex s : i.getSources()) {

	          final SDFAbstractVertex a = i.getAssociatedEdge(s).getTarget();

	          final SDFAbstractVertex b = i.getAssociatedEdge(s).getSource();

	          if ((a instanceof SDFVertex) || (a instanceof SDFRoundBufferVertex) || (a instanceof SDFBroadcastVertex)) {

	            inputRepVertexs.add(a);


	          }

	          if ((b instanceof SDFVertex) || (b instanceof SDFRoundBufferVertex) || (b instanceof SDFBroadcastVertex)) {

	            outputRepVertexs.add(b);


	          }

	        }

	        for (final SDFInterfaceVertex s : i.getSinks()) {

	          final SDFAbstractVertex a = i.getAssociatedEdge(s).getTarget();

	          final SDFAbstractVertex b = i.getAssociatedEdge(s).getSource();

	          if ((a instanceof SDFVertex) || (a instanceof SDFRoundBufferVertex) || (a instanceof SDFBroadcastVertex)) {

	            inputRepVertexs.add(a);

	            // p("3 output target " + a.getName());

	          }

	          if ((b instanceof SDFVertex) || (b instanceof SDFRoundBufferVertex) || (b instanceof SDFBroadcastVertex)) {

	            outputRepVertexs.add(b);


	          }

	        }

	      }
	    }
	}
}
